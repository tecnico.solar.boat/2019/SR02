/*
    Copyright (C) 2019  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <genieArduino.h>
#include <functions.h>
#include <strings.h>
#include "mctrl_torquedo.h"
#include <CAN_TSB.h>
#include <String.h>

#define VCONSTANT 0.2222
#define MOTORTHRESHOLD 120
#define KMHTOKNOTS 0.539956803

String pad_spaces = "   ";

//Global Variables 
int tab = 0 ;
float kwh_total = 0;
float kwh = 0;
float kwh_s = 0;
float time = 0;
int current_button = 0;
int last_button = 0;
int motor_threshold_current=MOTORTHRESHOLD;
//Genie and Can objects
Genie genie;
TSB_CAN m_can;
TSB_TORQUEDO MCTRL_THROTTLE_class;
CAN_message_t throttle_msg;
CAN_message_t throttle_threshold_msg;
CAN_message_t solar_relay_msg;
int current = 0;

//Interval Timer
IntervalTimer tabstimer;
IntervalTimer throttletimer;
IntervalTimer Energytimer;
IntervalTimer Send_data;

void setup() {
  //Begin Serial(Usb Connection) and Serial3(Screen Connection)
  Serial.begin(9600);
  Serial3.begin(9600);
  Serial1.begin(19200);
  //CAN begin
  Can0.begin(1000000);
  //Handler for CAN communication
  Can0.attachObj(&m_can);
  m_can.attachGeneralHandler();
  //genie begin in serial3
  genie.Begin(Serial3);
  //Reset pin definition
  pinMode(2,OUTPUT);
  //Reset Routine
  digitalWrite(2,HIGH);
  delay(1000);
  digitalWrite(2,LOW);
  digitalWrite(2,HIGH);

  //priority
  throttletimer.priority(0);

  //timers begin
  tabstimer.begin(write_tabs,1000000);
  throttletimer.begin(send_throttle,10000);
  Energytimer.begin(energy_calculation,1000000);
  Send_data.begin(Send_information,1000000);

  //hardware button interrupts
  pinMode(16, INPUT_PULLDOWN);
  pinMode(17, INPUT_PULLDOWN);
  pinMode(15, INPUT_PULLDOWN);
  pinMode(14, INPUT_PULLDOWN);
  attachInterrupt(16,tab_change_minus,RISING);
  attachInterrupt(17,tab_change_plus,RISING);
  attachInterrupt(15,reset_kwh,RISING);
  attachInterrupt(14,send_solarrelay_button,RISING);

  //multi switch interrupts
  pinMode(9, INPUT_PULLDOWN);
  pinMode(10, INPUT_PULLDOWN);
  pinMode(11, INPUT_PULLDOWN);
  pinMode(12, INPUT_PULLDOWN);
  pinMode(24, INPUT_PULLDOWN);
  pinMode(25, INPUT_PULLDOWN);
  pinMode(26, INPUT_PULLDOWN);
  pinMode(27, INPUT_PULLDOWN);
  pinMode(28, INPUT_PULLDOWN);
  attachInterrupt(9,multi1,RISING);
  attachInterrupt(10,multi2,RISING);
  attachInterrupt(11,multi3,RISING);
  attachInterrupt(12,multi4,RISING);
  attachInterrupt(24,multi6,RISING);
  attachInterrupt(25,multi7,RISING);
  attachInterrupt(26,multi8,RISING);
  attachInterrupt(27,multi9,RISING);
  attachInterrupt(28,multi10,RISING);

  genie.WriteContrast(15); //max 15 off 0
  genie.WriteObject(GENIE_OBJ_FORM,0,0);

  //Handler for screen serial communication
  genie.AttachEventHandler(myGenieEventHandler);
  
  //Prepare throttle
  MCTRL_THROTTLE_class.PrepareThrottleRequest();
  // Throttle stufffffff
  pinMode(5, OUTPUT);
  pinMode(6, INPUT);

  //genie.WriteObject(GENIE_OBJ_FORM,1, 0);
 }

void loop() {
  current = MCTRL_THROTTLE_class.getDesiredCurrent(); 
  //genie.DoEvents(); 
  genie.WriteStr(71,float_to_string(current) + " A" + pad_spaces);
}

void myGenieEventHandler(void){
  genieFrame Event;
  genie.DequeueEvent(&Event);
   // Remove the next queued event from the buffer, and process it below
  //If the cmd received is from a Reported Object, which occurs if a Read Object (genie.ReadOject) is requested in the main code, reply processed here.
  if (Event.reportObject.cmd == GENIE_REPORT_EVENT)
  {
    if (Event.reportObject.object == GENIE_OBJ_FORM)
    {
        tab = Event.reportObject.index; 
    }
  }
}

void send_throttle(){  
    throttle_msg.ext = 0;
    throttle_msg.id = 0x01;
    throttle_msg.len = 8;
    throttle_msg.buf[0] = lowByte(current);
    throttle_msg.buf[1] = highByte(current);
    Can0.write(throttle_msg);
}

void Send_information(){
  //send current threshold function
  send_current_threshold();
}
void send_current_threshold(){  
    throttle_threshold_msg.ext = 0;
    throttle_threshold_msg.id = 0x14;
    throttle_threshold_msg.len = 8;
    throttle_threshold_msg.buf[0] = lowByte(motor_threshold_current);
    throttle_threshold_msg.buf[1] = highByte(motor_threshold_current);
    Can0.write(throttle_threshold_msg);
}

void send_solarrelay_button(){
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 300ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > 300) 
  {
    solar_relay_msg.ext = 0;
    solar_relay_msg.id = 0x24;
    solar_relay_msg.len = 8;
    Can0.write(solar_relay_msg);
  }
 last_interrupt_time = interrupt_time;  
}

void write_tabs(){
  switch(tab)
  {
    case 0:
      write_tab_0();
      break;
    case 1:
      write_tab_1();
      break;
    case 3:
      write_tab_3();
      break;
    case 4:
      write_tab_4();
      break; 
    case 5:
      write_tab_5();
      break;
    case 6:
      write_tab_6();
      break;
    default:
      break;
  }
}

void write_tab_0(){
  //speed and knots
  genie.WriteObject(GENIE_OBJ_LED_DIGITS,0,m_can.foils.veloc);
  genie.WriteObject(GENIE_OBJ_LED_DIGITS,1,m_can.foils.veloc*KMHTOKNOTS);

  genie.WriteStr(0,int_to_string(kwh_s) + " Wh" + pad_spaces);
  genie.WriteStr(55,float_to_string(time) + " s" + pad_spaces);
  genie.WriteStr(1,int_to_string(m_can.bms.solar_current*m_can.bms.voltage) + " W" + pad_spaces);
  genie.WriteStr(2,int_to_string(m_can.bms.voltage*m_can.bms.current) + " W" + pad_spaces);
  genie.WriteStr(3,int_to_string(m_can.motor.voltage*(m_can.motor.input1_current + m_can.motor.input2_current)) + " W" + pad_spaces);

  genie.WriteStr(4,float_to_string(m_can.bms.voltage) + " V" + pad_spaces);
  genie.WriteStr(5,float_to_string(m_can.bms.current) + " A" + pad_spaces);
  genie.WriteStr(6,float_to_string(m_can.bms.solar_current) + " A" + pad_spaces);
  genie.WriteStr(7,float_to_string(get_highest_temperature()) + " C" + pad_spaces);
  genie.WriteStr(8,"M1: " + float_to_string(m_can.motor.mtemperature1) + " C");
  genie.WriteStr(126,"V1: " + float_to_string(m_can.motor.temperature1) + " C" + pad_spaces);
  genie.WriteStr(127,"M2: " + float_to_string(m_can.motor.mtemperature2) + " C");
  genie.WriteStr(128,"V2: " + float_to_string(m_can.motor.temperature2) + " C" + pad_spaces);
  //warnings
  genie.WriteStr(10,float_to_string(m_can.bms.soc) + " %" + pad_spaces);

  tab0_status_decode();
  tab0_warnings_decode();

  genie.WriteObject(GENIE_OBJ_TANK,0,(m_can.bms.soc/100));
}
void write_tab_1(){
  for(int i = 0; i < 12 ;i++){
    int voltage = m_can.bms.cellvoltages[i]*100*0.22222;
    int write = (i << 8) | voltage; 
    genie.WriteObject(GENIE_OBJ_SPECTRUM,0,write);
    genie.WriteStr(i + 11,float_to_string(m_can.bms.cellvoltages[i]) + " V");
  }

  genie.WriteStr(64,float_to_string(m_can.bms.current) + " A" + pad_spaces);
  if(m_can.bms.current < 0){
    genie.WriteObject(GENIE_OBJ_GAUGE,0,abs(m_can.bms.current)/4);
  }else{
    genie.WriteObject(GENIE_OBJ_GAUGE,1,m_can.bms.current/4);
  }
  

  tab1_status_decode();
  tab1_balancing_decode();
  tab1_warnings_decode();

  genie.WriteStr(24,float_to_string(m_can.bms.soc) + " %" + pad_spaces);
  genie.WriteStr(25,float_to_string(m_can.bms.voltage) + " V" + pad_spaces);
  genie.WriteStr(26,float_to_string(m_can.bms.temperatures[3]) + " C" + pad_spaces);
}

void write_tab_3(){
  for(int i = 0; i < 13 ;i++){
    int temperature = m_can.bms.temperatures[i];
    int write = (i << 8) | temperature; 
    genie.WriteObject(GENIE_OBJ_SPECTRUM,1,write);
  }

  genie.WriteStr(39,int_to_string(m_can.bms.temperatures[0]) + " C" + pad_spaces);
  genie.WriteStr(42,int_to_string(m_can.bms.temperatures[1]) + " C" + pad_spaces);
  genie.WriteStr(41,int_to_string(m_can.bms.temperatures[2]) + " C" + pad_spaces);
  genie.WriteStr(40,int_to_string(m_can.bms.temperatures[3]) + " C" + pad_spaces);
  genie.WriteStr(43,int_to_string(m_can.bms.temperatures[4]) + " C" + pad_spaces);
  genie.WriteStr(45,int_to_string(m_can.bms.temperatures[5]) + " C" + pad_spaces);
  genie.WriteStr(46,int_to_string(m_can.bms.temperatures[6]) + " C" + pad_spaces);
  genie.WriteStr(51,int_to_string(m_can.bms.temperatures[7]) + " C" + pad_spaces);
  genie.WriteStr(49,int_to_string(m_can.bms.temperatures[8]) + " C" + pad_spaces);
  genie.WriteStr(48,int_to_string(m_can.bms.temperatures[9]) + " C" + pad_spaces);
  genie.WriteStr(47,int_to_string(m_can.bms.temperatures[10]) + " C" + pad_spaces);
  genie.WriteStr(50,int_to_string(m_can.bms.temperatures[11]) + " C" + pad_spaces);
  genie.WriteStr(52,int_to_string(m_can.bms.temperatures[12]) + " C" + pad_spaces);

  genie.WriteStr(62,float_to_string(m_can.motor.temperature1) + " C" + pad_spaces);
  genie.WriteStr(63,float_to_string(m_can.motor.temperature2) + " C" + pad_spaces);

  genie.WriteStr(59,float_to_string(m_can.motor.mtemperature1) + " C" + pad_spaces);
  genie.WriteStr(61,float_to_string(m_can.motor.mtemperature2) + " C" + pad_spaces);


  genie.WriteStr(113,float_to_string(0) + " C" + pad_spaces);
  genie.WriteStr(114,float_to_string(0) + " C" + pad_spaces);
  genie.WriteStr(115,float_to_string(0) + " C" + pad_spaces);
  genie.WriteStr(116,float_to_string(0) + " C" + pad_spaces);

}
void write_tab_4(){
  for(int i = 0; i < 5 ;i++){
    int solar_voltage = m_can.bms.solar_voltages[i];
    int write = (i << 8) | solar_voltage; 
    genie.WriteObject(GENIE_OBJ_SPECTRUM,2,write);
  }
  genie.WriteStr(53,float_to_string(m_can.bms.solar_voltages[0]) + " V" + pad_spaces);
  genie.WriteStr(72,float_to_string(m_can.bms.solar_voltages[1]) + " V" + pad_spaces);
  genie.WriteStr(73,float_to_string(m_can.bms.solar_voltages[2]) + " V" + pad_spaces);
  genie.WriteStr(74,float_to_string(m_can.bms.solar_voltages[3]) + " V" + pad_spaces);
  genie.WriteStr(75,float_to_string(m_can.bms.solar_voltages[4]) + " V" + pad_spaces);

  genie.WriteStr(76,float_to_string(m_can.bms.solar_current) + " A" + pad_spaces);
}

void write_tab_5(){
  //motor 1
  genie.WriteStr(54,float_to_string(m_can.motor.input1_current) + " A" + pad_spaces);
  genie.WriteStr(56,float_to_string(m_can.motor.motor1_current) + " A" + pad_spaces);
  genie.WriteStr(57,float_to_string(m_can.motor.rpm/5) + " RPM" + pad_spaces);
  genie.WriteStr(58,float_to_string(m_can.motor.voltage) + " V" + pad_spaces);
  genie.WriteStr(60,float_to_string(m_can.motor.mtemperature1) + " C" + pad_spaces);

  //motor 2
  genie.WriteStr(65,float_to_string(m_can.motor.input2_current) + " A" + pad_spaces);
  genie.WriteStr(66,float_to_string(m_can.motor.motor2_current) + " A" + pad_spaces);
  genie.WriteStr(67,float_to_string(m_can.motor.rpm/5) + " RPM" + pad_spaces);
  genie.WriteStr(68,float_to_string(m_can.motor.voltage) + " V" + pad_spaces);
  genie.WriteStr(69,float_to_string(m_can.motor.mtemperature2) + " C" + pad_spaces);

  //common parameters
  //genie.WriteStr(71,float_to_string(current) + " A");
  genie.WriteStr(70,int_to_string(motor_threshold_current) + " A" + pad_spaces);
}

void write_tab_6(){
}

String float_to_string(float value){
  String mystring;
  return String(value);
}

String int_to_string(int value){
  String mystring;
  return String(value);
}

void tab_change_plus(){
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 300ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > 300) 
  {
    if(tab+1==7){
      tab=-1;
    }
    genie.WriteObject(GENIE_OBJ_FORM, tab+1, 0);
    tab++;
  }
 last_interrupt_time = interrupt_time;
}

void tab_change_minus(){
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 300ms, assume it's a bounce and ignore
  if (interrupt_time - last_interrupt_time > 300) 
  {
    if(tab==0){
      tab=7;
    }
    genie.WriteObject(GENIE_OBJ_FORM, tab-1, 0);
    tab--;
  }
 last_interrupt_time = interrupt_time;
}

void energy_calculation(){
  time++;
  if(time>0){ 
    kwh_total += (-1 * m_can.bms.voltage * m_can.bms.current);
    kwh = kwh_total / time;
    kwh_s += kwh / 3600;

  }
}

void reset_kwh(){
  time=0;
  kwh =0;
  kwh_total=0;
  kwh_s=0;
}

void tab0_warnings_decode(){

  if(bitRead(m_can.bms.warnings,0)){
      genie.WriteStr(9,"Over Current" + pad_spaces);
  }
  if(bitRead(m_can.bms.warnings,1)){
      genie.WriteStr(123,"Over Temperature" + pad_spaces);
  }
  if(bitRead(m_can.bms.warnings,2)){
      genie.WriteStr(124,"Over Voltage" + pad_spaces);
  }
  if(bitRead(m_can.bms.warnings,3)){
      genie.WriteStr(125,"Under Voltage" + pad_spaces);
  }

}
void tab1_warnings_decode(){
  if(bitRead(m_can.bms.warnings,0)){
      genie.WriteStr(28,"Over Current" + pad_spaces);
  }
  if(bitRead(m_can.bms.warnings,1)){
      genie.WriteStr(29,"Over Temperature" + pad_spaces);
  }
  if(bitRead(m_can.bms.warnings,2)){
      genie.WriteStr(27,"Over Voltage" + pad_spaces);
  }
  if(bitRead(m_can.bms.warnings,3)){
      genie.WriteStr(27,"Under Voltage" + pad_spaces);
  }
}


void tab1_status_decode(){

    if(bitRead(m_can.bms.status,0)==1){
      genie.WriteStr(23,"Charging" + pad_spaces);
    }
    if(bitRead(m_can.bms.status,1)==1){
      genie.WriteStr(23,"Discharging" + pad_spaces);
    }
    if(bitRead(m_can.bms.status,2)==1){
      genie.WriteObject(GENIE_OBJ_LED,4,1);
    }
    if(bitRead(m_can.bms.status,3)==1){
      genie.WriteObject(GENIE_OBJ_LED,3,1);
    }
    if(bitRead(m_can.bms.status,4)==1){
      genie.WriteObject(GENIE_OBJ_LED,5,1);
    }else{
      genie.WriteObject(GENIE_OBJ_LED,5,0);
    }
    if(bitRead(m_can.bms.status,5)==1){
      genie.WriteObject(GENIE_OBJ_LED,6,1);
    }else{
      genie.WriteObject(GENIE_OBJ_LED,6,0);
    }

    if(bitRead(m_can.bms.status,6)==1){
      genie.WriteObject(GENIE_OBJ_LED,7,1);     
    }else{
      genie.WriteObject(GENIE_OBJ_LED,7,0);
    }
}

void tab0_status_decode(){

    if(bitRead(m_can.bms.status,4)==1){
      genie.WriteObject(GENIE_OBJ_LED,0,1);
    }else{
      genie.WriteObject(GENIE_OBJ_LED,0,0);
    }
    if(bitRead(m_can.bms.status,5)==1){
      genie.WriteObject(GENIE_OBJ_LED,2,1);  
    }else{
      genie.WriteObject(GENIE_OBJ_LED,2,0);
    }
    if(bitRead(m_can.bms.status,6)==1){
      genie.WriteObject(GENIE_OBJ_LED,1,1);   
    }else{
      genie.WriteObject(GENIE_OBJ_LED,1,0);
    }
}

void tab1_balancing_decode(){
  for(int i = 0; i < 12 ;i++){
    if(bitRead(m_can.bms.cells_balanc,i)==1){
      genie.WriteObject(GENIE_OBJ_LED,i+9,1);
    }else{
      genie.WriteObject(GENIE_OBJ_LED,i+9,0);
    }
  }
}

void multi1(){
  current_button=1;
  if(last_button==0){
    last_button=1;
    return;
  }
  if(last_button==10){
    motor_threshold_current++;
  }
  if(last_button==2){
    if(motor_threshold_current>0)
      motor_threshold_current--;
  }
  last_button=1;
}
void multi2(){
  current_button=2;
  if(last_button==0){
    last_button=2;
    return;
  }
  if(last_button==1){
    motor_threshold_current++;
  }
  if(last_button==3){
    if(motor_threshold_current>0)
      motor_threshold_current--;
  }
  last_button=2;
}
void multi3(){
  current_button=3;
  if(last_button==0){
    last_button=3;
    return;
  }
  if(last_button==2){
    motor_threshold_current++;
  }
  if(last_button==4){
    if(motor_threshold_current>0)
      motor_threshold_current--;
  }
  last_button=3;
}
void multi4(){
  current_button=4;
  if(last_button==0){
    last_button=4;
    return;
  }
  if(last_button==3){
    motor_threshold_current++;
  }
  if(last_button==6){
    if(motor_threshold_current>0)
      motor_threshold_current--;
  }
  last_button=4;
}
void multi5(){
  current_button=5;
}
void multi6(){
  current_button=6;
  if(last_button==0){
    last_button=6;
    return;
  }
  if(last_button==4){
      motor_threshold_current++;
  }
  if(last_button==7){
    if(motor_threshold_current>0)
      motor_threshold_current--;
  }
  last_button=6;
}
void multi7(){
  current_button=7;
  if(last_button==0){
    last_button=7;
    return;
  }
  if(last_button==6){
    motor_threshold_current++;
  }
  if(last_button==8){
    if(motor_threshold_current>0)
      motor_threshold_current--;
  }
  last_button=7;
}
void multi8(){
  current_button=8;
  if(last_button==0){
    last_button=8;
    return;
  }
  if(last_button==7){
    motor_threshold_current++;
  }
  if(last_button==9){
    if(motor_threshold_current>0)
      motor_threshold_current--;
  }
  last_button=8;
}
void multi9(){
  current_button=9;
  if(last_button==0){
    last_button=9;
    return;
  }
  if(last_button==8){
    motor_threshold_current++;
  }
  if(last_button==10){
    if(motor_threshold_current>0)
      motor_threshold_current--;
  }
  last_button=9;
}
void multi10(){
  current_button=10;
  if(last_button==0){
    last_button=10;
    return;
  }
  if(last_button==9){
    motor_threshold_current++;
  }
  if(last_button==1){
    if(motor_threshold_current>0)
      motor_threshold_current--;
  }
  last_button=10;
}
float get_highest_temperature(){
  int max_temp = 0;
  for (int i = 0; i < 17; i++)
  {
    if(m_can.bms.temperatures[i]>max_temp){
      max_temp=m_can.bms.temperatures[i];
    }
  }
  
  return max_temp;
}
