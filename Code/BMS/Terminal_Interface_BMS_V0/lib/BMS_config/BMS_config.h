/*
    Copyright (C) 2019  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef BMS_CONFIG_H
#define BMS_CONFIG_H

#include <Arduino.h>  // typedefs use types defined in this header file.
#include "LTC681x.h"

/*****************************************
 ********** Variaveis globais  ***********
*****************************************/
// SOC
extern bool isFull;
extern bool chargingAllowed;

// Balancing
extern bool timeToBalance;
extern bool balancingAllowed;
extern bool isBalancing;
extern uint16_t minimumCellVoltage;

// Voltage sensors
extern bool hasVoltageCharger;
extern bool hasVoltageMotor;

// Discharge
extern bool motorRelayON;

#define inbalance_thershold 500 //500 means 50 mV

/*****************************************
 ********** LTC6811-2 Chip Select ********
*****************************************/
#define LTC_CS 2 


/*****************************************
 *********** LTC6811-2 Parameters ********
*****************************************/

#define ENABLED 1
#define DISABLED 0
#define DATALOG_ENABLED 1
#define DATALOG_DISABLED 0

const uint8_t TOTAL_IC = 1; // number of ICs in the daisy chain

const uint16_t MEASUREMENT_LOOP_TIME = 500; //milliseconds(mS)

/*****************************************
 *********** ADC configurations **********
*****************************************/
// ADC_OPT and ADC_CONVERSION_MODE sets the ADC mode
const uint8_t ADC_OPT = ADC_OPT_0; // Selects Modes 27kHz, 7kHz, 422Hz or 26Hz with MD[1:0] Bits in ADC Conversion Commands

// Sets to 26 Hz (filterded mode) sice ADC_OPT = 0
const uint8_t ADC_CONVERSION_MODE = MD_26HZ_2KHZ;

// Do not permit discharfe while measuring
const uint8_t ADC_DCP = DCP_DISABLED; 

// Measure all cells 
const uint8_t CELL_CH_TO_CONVERT = CELL_CH_ALL; 

// Measure all  GPIOS and 2nd reference
const uint8_t AUX_CH_TO_CONVERT = AUX_CH_ALL; 

// Measure SC (Sum of all cells), Internal temperature, Analog supply voltage
// and Digital supply voltage
const uint8_t STAT_CH_TO_CONVERT = STAT_CH_ALL;

/*******************************************
 *********** Under / Over voltage **********
 ************** configuration **************
********************************************/
const uint16_t OV_THRESHOLD = 42000; // Over voltage threshold ADC Code. LSB = 0.0001
const uint16_t UV_THRESHOLD = 30000; // Under voltage threshold ADC Code. LSB = 0.0001

//Loop Measurement Setup These Variables are ENABLED or DISABLED Remember ALL CAPS
const uint8_t WRITE_CONFIG = DISABLED; // This is ENABLED or DISABLED
const uint8_t READ_CONFIG = DISABLED; // This is ENABLED or DISABLED
const uint8_t MEASURE_CELL = ENABLED; // This is ENABLED or DISABLED
const uint8_t MEASURE_AUX = DISABLED; // This is ENABLED or DISABLED
const uint8_t MEASURE_STAT = DISABLED; //This is ENABLED or DISABLED
const uint8_t PRINT_PEC = DISABLED; //This is ENABLED or DISABLED

/*****************************************
 ***************** Macros ****************
*****************************************/
//! Set "pin" low
//! @param pin pin to be driven LOW
#define output_low(pin)   digitalWrite(pin, LOW)
//! Set "pin" high
//! @param pin pin to be driven HIGH
#define output_high(pin)  digitalWrite(pin, HIGH)
//! Return the state of pin "pin"
//! @param pin pin to be read (HIGH or LOW).
//! @return the state of pin "pin"
#define input(pin)        digitalRead(pin)


#endif  // BMS_CONFIG_H
