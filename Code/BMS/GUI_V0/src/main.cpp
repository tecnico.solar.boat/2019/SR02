/*
    Copyright (C) 2019  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <Arduino.h>
#include <SPI.h>
#include <stdint.h>
#include "BMS_config.h"
#include "LT_SPI.h"
#include "LTC681x.h"
#include "LTC6811.h"
#include "BMS.h"
#include "tsb_serial.h"

/*****************************************
 ********** Global Variables*  ***********
*****************************************/
// SOC
bool isFull = true;
bool chargingAllowed = false;
bool isCharging = false;

// Balancing
bool timeToBalance = false;
bool balancingAllowed = true;
bool isBalancing = false;
uint16_t minimumCellVoltage = OV_THRESHOLD; // Just for inicializing BMS_needs_balance() chages this value

// Voltage sensors
bool hasVoltageCharger = false;
bool hasVoltageMotor = false;

// Discharge
bool motorRelayON = false;

// LTC Status Variable
uint16_t LTC_Status = 0;

// BMS Warning Variable
uint8_t warnings = 0;

// BMS Timer
IntervalTimer BMS_Timer;

// BMS Serial Messages:
TSB_SERIAL serial_interface;
tsb_serial_msg_t cellvoltages0;
tsb_serial_msg_t cellvoltages1;
tsb_serial_msg_t cellvoltages2;

/******************************************************
 *** Global Battery Variables received from 681x commands
 These variables store the results from the LTC6811
 register reads and the array lengths must be based
 on the number of ICs on the stack
 ******************************************************/

cell_asic bms_ic[TOTAL_IC];

void BMS_Timer_callback();
void initBMSSerial();
void updateBMSSerial();

/*!**********************************************************************
 \brief  Inititializes hardware and variables
 ***********************************************************************/
void setup()
{
	Serial.begin(9600);
	BMS_init(bms_ic);
	BMS_selfCheck(bms_ic, &LTC_Status);
	if ( LTC_Status != 0 ) {
		// Mandar erro Serial
		// Buzzer e o crl
	}
	initBMSSerial();
	BMS_Timer.begin(BMS_Timer_callback, 1000000);
}

/*!*********************************************************************
	\brief main loop
***********************************************************************/
uint32_t current_time = millis();
uint32_t last_time = 0;
void loop()
{
	current_time = millis();
	if (current_time - last_time > 1000 ) {
		updateBMSSerial();
		last_time = current_time;
	}
	
}


void BMS_Timer_callback(){
	int8_t error = 0;
	uint8_t OV_UV = NONE;

	// Read Cells and current sensors
	error = BMS_readCells_GPIO_1_2(bms_ic, &LTC_Status);
	while (error != 0) {
		error = BMS_readCells_GPIO_1_2(bms_ic, &LTC_Status);
	}
	
	// // Read SOC, LTC temperature, VregA, VregD and GPIO 3 to 5
	// BMS_read_aux( bms_ic, &LTC_Status);
	
	// // Checks if values are ok
	// if ( (OV_UV = BMS_OV_UV( bms_ic, &warnings, &LTC_Status)) != NONE) {
	// 	// Emergency shit
	// 	// Cut Charger and Solar Relays
	// 	if (OV_UV == OVER_VOLTAGE ) {
	// 		// Turn on discharge on OV cells
	// 		BMS_discharge_OV( bms_ic );
	// 		isBalancing = true;
	// 	}	

	// 	//Send message and reset WDT
	// 	return;
	// }
	// else{
	// 	// Is full?
	// 	if ( bms_ic[0].stat.stat_codes[0] >= FULL_BATTERY || BMS_is_cell_charged( bms_ic ) ) {
	// 		isFull = true;
	// 		// Open charger and Solar Relay
	// 		return;
	// 	}
	// 	else{
	// 		isFull = false;
	// 		// Is balancing?
	// 		if ( isBalancing == true ) {
	// 			BMS_check_balance( bms_ic, minimumCellVoltage );
	// 			if ( BMS_any_balancing( bms_ic ) == false) {
	// 				isBalancing = false;
	// 			}
	// 			return;
	// 		}
	// 		else{
	// 			if (timeToBalance && balancingAllowed) {
	// 				if ( BMS_needs_balance( bms_ic, &minimumCellVoltage ) ) {
	// 					// Open solar and charger relay
	// 					isBalancing = true;
	// 					BMS_balance( bms_ic, minimumCellVoltage );
	// 					return;
	// 				}
	// 				return;
	// 			}
	// 			else{
	// 				if (chargingAllowed == false) {
	// 					// open solar and charger relay
	// 					return;
	// 				}
	// 				else{
	// 					if ( (hasVoltageCharger == true) && (isCharging == false) ) {
	// 						// Close charger relay and open solar relay
	// 						return;
	// 					}
	// 					else{
	// 						if ( isCharging == false ) {
	// 							// Open charger relay and close solar relay
	// 							return;
	// 						}
	// 						else{
	// 							return;
	// 						}	
	// 					}	
	// 				}	
	// 			}
	// 		}
	// 	}	
	// }

}


void initBMSSerial(){
	cellvoltages0.addr = 0x00;
	cellvoltages1.addr = 0x00;
	cellvoltages2.addr = 0x00;

	cellvoltages0.data_id = 0x03;
	cellvoltages1.data_id = 0x04;
	cellvoltages2.data_id = 0x05;

	cellvoltages0.data_size = 8;
	cellvoltages1.data_size = 8;
	cellvoltages2.data_size = 8;
}

void updateBMSSerial(){
	for(int i = 0, j = 0; i < 4 ; i++, j=j+2)
	{
		cellvoltages0.data[j] = highByte( bms_ic[0].cells.c_codes[i] );
		cellvoltages0.data[j+1] = lowByte( bms_ic[0].cells.c_codes[i] );

		cellvoltages1.data[j] = highByte( bms_ic[0].cells.c_codes[i+4] );
		cellvoltages1.data[j+1] = lowByte( bms_ic[0].cells.c_codes[i+4] );

		cellvoltages2.data[j] = highByte( bms_ic[0].cells.c_codes[i+8] );
		cellvoltages2.data[j+1] = lowByte( bms_ic[0].cells.c_codes[i+8] );
	}

	serial_interface.send_msg(cellvoltages0);
	serial_interface.send_msg(cellvoltages1);
	serial_interface.send_msg(cellvoltages2);
}