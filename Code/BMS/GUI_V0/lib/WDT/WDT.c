/*
    Copyright (C) 2019  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "WDT.h"


void WDT_setup(){
	// Setup WDT
    noInterrupts();                                         // don't allow interrupts while setting up WDOG
    WDOG_UNLOCK = WDOG_UNLOCK_SEQ1;                         // unlock access to WDOG registers
    WDOG_UNLOCK = WDOG_UNLOCK_SEQ2;
    delayMicroseconds(1);                                   // Need to wait a bit..

    // for this demo, we will use 1 second WDT timeout (e.g. you must reset it in < 1 sec or a boot occurs)
    // With a prescale of 0x400 the WDT timer runs at 7.2 MHz so to do 1 s we need 7 200 000 ticks -> 0x006DDD00 
    WDOG_TOVALH = 0x006d;
    WDOG_TOVALL = 0xdd00;

    // This sets prescale clock so that the watchdog timer ticks at 7.2MHz
    WDOG_PRESC  = 0x400;

    // Set options to enable WDT. You must always do this as a SINGLE write to WDOG_CTRLH
    WDOG_STCTRLH |= WDOG_STCTRLH_ALLOWUPDATE |
        WDOG_STCTRLH_WDOGEN | WDOG_STCTRLH_WAITEN |
        WDOG_STCTRLH_STOPEN | WDOG_STCTRLH_CLKSRC;
        // To call WDOG interrupt also do an or with:
        //WDOG_STCTRLH_IRQRSTEN;            // Tell WDOG to trigger the interrupt
	// and call (void watchdog_isr() need to be difined):
	//NVIC_ENABLE_IRQ(IRQ_WDOG);       		// enable the call to the watchdog_isr function

    
    interrupts();
}

void WDT_kick(){
	noInterrupts();                                     
    WDOG_REFRESH = 0xA602;
    WDOG_REFRESH = 0xB480;
    interrupts();
}

void WDT_disable(){
	NVIC_DISABLE_IRQ(IRQ_WDOG);
	noInterrupts(); // don't allow interrupts while setting up WDOG
	WDOG_UNLOCK = WDOG_UNLOCK_SEQ1; // unlock access to WDOG registers
	WDOG_UNLOCK = WDOG_UNLOCK_SEQ2;
	delayMicroseconds(1); // Need to wait a bit..

	// Set options to enable WDT. You must always do this as a SINGLE write to WDOG_CTRLH
	WDOG_STCTRLH = WDOG_STCTRLH_ALLOWUPDATE;
	
	interrupts();
}