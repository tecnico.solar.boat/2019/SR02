/*
    Copyright (C) 2019  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/



/*Adc testing module 30/03
Test the adc by measuring the voltage and current provided by the dummy cell and
one
Current drain is expected to be around 14,38 due to the potentiometer's
limitation
*/

#include <Arduino.h>
#include <TestBench.h>
#include <TSB_datalogger.h>

TestBench Tb;

//String Data;

void setup() {

  Serial.begin(9600);
  Wire.begin();
  Tb.begin();
  initSD();
  initLOG();
  delay(10000);

}
void loop() {

  Data = Tb.discharge();
  if(Serial)Serial.println(Data);
  writeLogEntry();
  delay(1500); //Mais rapido da problema
}

