/*
    Copyright (C) 2019  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <Adafruit_ADS1015.h>
#include <INA.h>
#include "Filter.h"
#include <Wire.h>
#include <math.h>
#include <tiPortExpander.h>
#include <i2cPot.h>

//ADDRS
#define INA_ADDR 0x44
#define POT_ADDR 0x52
#define ADC_ADDR 0x48
#define GPIO_ADDR 0x3B
#define THERMISTOR_PIN A1

// INA
#define INAB_CURR 141.3
#define INAB_R 300.0
#define INAC_CURR 20.0
#define INAC_R 1000.0
#define INA_SELECT 1 //1 FOR BAT 0 FOR CHG

//DISCHARGE
#define VOLT_THR_LOW 2.95
#define VOLT_THR_HIGH 4.2
#define TEMP_THR_HIGH 58.0
#define HIGH_TEMP_FINISH 0
#define NORMAL_FINISH 1
#define POT_LEVEL 0x66 //4B for 16.8A 0x44 for 2c

//GPIO
#define LEDC 0
#define LED1 1
#define LED2 2
#define LED3 3
#define RL_CHG 6
#define RL_BAT 7

//Termistor
#define ADC_16_BIT 0.000188
#define VCC 3.3
#define ARD_ADC_BITS 1023.0

#define DESIRED_CUR 33.6

//Thermistor
#define A 0.003354016
#define B 0.000300131
#define C 0.0000050851649437909
#define D 0.000000218565
#define R25  10000.0
#define R0 10000.0



class TestBench
{
  public:
    TestBench();
    void begin();

    String discharge();

  private:
    String measure(int ina_to_read);
    float temperatures(int);
    float current_flowing(int ina_to_read);
    float voltage();
    void finished_code(int num);
    int _level;
    float _current;
    float _voltage;
    float _temperature;
};
