/*
Library for interacting with the I2C Digital Potentiometer
ISL23315WFUZ-T7A
Created by Tiago Miotto 13/11/18
*/


#ifndef i2cPot_h
#define i2cPot_h

#include "Arduino.h" // do i need this with rfduino?
#include "Wire.h"

#define DEFAULT_ADDR 0x50
#define DEFAULT_LEVEL 0x7f

class i2cPot {
public:
  i2cPot();
  i2cPot(int address);
  int writeLevel(int level);
  int readLevel();

private:
  int _deviceAddress;
  int16_t _currentLevel;
};

#endif
