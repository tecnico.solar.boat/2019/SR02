/*
Library for interacting with the I2C Digital Potentiometer
ISL23315WFUZ-T7A
Created by Tiago Miotto 13/11/18
*/

#include "Arduino.h" // do i need this with rfduino?
#include "Wire.h"
#include "i2cPot.h"

// default constructor automatically sets address to ISL23315WFUZ-T7A address
i2cPot::i2cPot()
{
    _currentLevel = DEFAULT_LEVEL;
    _deviceAddress = DEFAULT_ADDR;
}

// for configuring your own address with the ISL23315WFUZ-T7A
i2cPot::i2cPot(int address)
{
    _currentLevel = DEFAULT_LEVEL;
    _deviceAddress = address;
}
//Sets the wiper to a given level
int i2cPot::writeLevel(int level)
{
    Wire.beginTransmission(_deviceAddress); // transmit to device #addr (0x2c)
    // device address is specified in datasheet
    Wire.write(byte(0x00)); // sends instruction byte // Essa instrução é pra
                            // escrever no Wiper Registry
    Wire.write(level);      // sends potentiometer value 
    _currentLevel=level;
    if (Wire.endTransmission() == 0)
    {
        return -1;
    }
    else
    {
        return 1;
    }
}
//Reads the current level of the potentiometer
int i2cPot::readLevel()
{                                           //Not tested
    Wire.beginTransmission(_deviceAddress); // transmit to device #addr (0x2c)
    // device address is specified in datasheet
    Wire.write(byte(0x00)); // sends instruction byte // Essa instrução é pra
    // escrever no Wiper Registry
    Wire.endTransmission();
    Wire.requestFrom(_deviceAddress, 2); // request 6 bytes from slave device #8
    char total;
    while (Wire.available())
    {                         // slave may send less than requested
        char c = Wire.read(); // receive a byte as character
        total += c;
    }
    return int(total);
}