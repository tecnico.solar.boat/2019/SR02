/*
    Copyright (C) 2019  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSerialPort>
#include <QSerialPortInfo>
#include <string>
#include <QDebug>
#include <QMessageBox>
#include <QTimer>
#include <QPixmap>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
    {
        ui->setupUi(this);
        setupgui();
        //check for board connection and disconnection
        QTimer *dataTimer = new QTimer(this);
        QObject::connect(dataTimer, &QTimer::timeout,[=](){scanports();});
        dataTimer->start(100);

        //Calls sendserial() when upload button is pressed
        QObject::connect(ui->upload_button,SIGNAL(clicked()),this,SLOT(sendSerial()));

        //makes read_values true when read button is presses
        QObject::connect(ui->read_button,SIGNAL(clicked()),this,SLOT(readvalues_enable()));

        //Connects board when connect button is pressed
        QObject::connect(ui->connect_button,SIGNAL(clicked()),this,SLOT(board_connection()));

        //Disconnects board when disconnect button is pressed
        QObject::connect(ui->disconnect_button,SIGNAL(clicked()),this,SLOT(board_disconnection()));


    }

MainWindow::~MainWindow()
{
    delete ui;
    if(board->isOpen()){
        board->close(); //    Close the serial port if it's open.
    }
}

//Setups all interfaces
void MainWindow::setupgui()
{
    //setup board
    board = new QSerialPort(this);
    board_is_available = false;

    //setup protocol messages
    MSG_START_BYTE.append('\xAC');
    MSG_END_BYTE.append('\xAD');

    STATUS_ID.append('\x00');
    SOC_V_I_ID.append('\x00');
    CELLS_0_ID.append('\x00');
    CELLS_1_ID.append('\x00');
    CELLS_2_ID.append('\x00');
    SOLAR_0_ID.append('\x00');
    SOLAR_1_ID.append('\x00');
    TEMP_0_ID.append('\x00');
    TEMP_1_ID.append('\x00');
    TEMP_2_ID.append('\x00');

    VALUES_1_ID.append('\x00');
    VALUES_2_ID.append('\x00');
    VALUES_3_ID.append('\x00');

    STATUS_ID.append('\x01');
    SOC_V_I_ID.append('\x02');
    CELLS_0_ID.append('\x03');
    CELLS_1_ID.append('\x04');
    CELLS_2_ID.append('\x05');
    SOLAR_0_ID.append('\x06');
    SOLAR_1_ID.append('\x07');
    TEMP_0_ID.append('\x08');
    TEMP_1_ID.append('\x09');
    TEMP_2_ID.append('\x0A');

    VALUES_1_ID.append('\x0C');
    VALUES_2_ID.append('\x0D');
    VALUES_3_ID.append('\x0E');

    //setup buffers
    firstBuffer = "";
    secondBuffer = "";

    //setup variables
    battery_voltage = 0.0;
    soc = 0.0;

    soc_label_gui = "";
    battery_voltage_label_gui = "";
    pack_temp_label_gui = "";
    bat_current_label_gui = "";
    solar_current_label_gui = "";

    //setup voltage vectors
    x_cells.resize(12);
    y_voltages.resize(12);
    x_bat_current.resize(1);
    y_bat_current.resize(1);
    x_solar_current.resize(1);
    y_solar_current.resize(1);
    x_temperatures.resize(12);
    y_temperatures.resize(12);
    x_SP_voltages.resize(5);
    y_SP_voltages.resize(5);
    bar_voltage_label.resize(12);
    bar_temperature_label.resize(12);
    bar_solar_voltage_label.resize(5);
    balancing_cells.resize(12);
    y_voltages_balancing.resize(12);
    temperatures.resize(4);
    heatsink_temp.resize(2);

    //setup balancing cells vector
    for(int i=0;i<x_cells.size();i++){
        balancing_cells[i] = false;
    }

    //setup voltage graph
    ui->voltage_graph->xAxis->setRange(0, 12);
    ui->voltage_graph->xAxis->setTickLabels(false);
    ui->voltage_graph->xAxis->setSubTicks(false);
    ui->voltage_graph->axisRect()->setupFullAxesBox();
    ui->voltage_graph->xAxis->setSubTicks(true);
    ui->voltage_graph->yAxis->setRange(2, 5);


    //setup voltage bars
    voltage_bars = new QCPBars(ui->voltage_graph->xAxis,ui->voltage_graph->yAxis);

    //sets bars location
    double position = 0.5;
    for(int i = 0;i < x_cells.size(); i++){
        x_cells[i] = position;
        position += 1;
    }

    //setup highlighted voltage bars (for balancing)
    balancing_bars = new QCPBars(ui->voltage_graph->xAxis,ui->voltage_graph->yAxis);

    //Setup top of bar voltage labels
    position = (0.041666666666666);
    for(int i = 0;i < x_cells.size(); i++){
        bar_voltage_label[i] = new QCPItemText(ui->voltage_graph);
        bar_voltage_label[i]->setPositionAlignment(Qt::AlignTop|Qt::AlignHCenter);
        bar_voltage_label[i]->setTextAlignment(Qt::AlignHCenter);
        bar_voltage_label[i]->position->setType(QCPItemPosition::ptAxisRectRatio);
        bar_voltage_label[i]->position->setCoords(position, 0.9);
        bar_voltage_label[i]->setText("0.000");
        position += 2*(0.041666666666666);
    }

    //setup current bars location
    x_bat_current[0] = 0.5;
    x_solar_current[0] = 0.5;

    //setup battery current graph
    ui->bat_current_graph->yAxis->setRange(0, 1);
    ui->bat_current_graph->xAxis->setRange(-400, 400);
    ui->bat_current_graph->yAxis->setVisible(false);

    //setup battery current bar
    bat_current_bar = new QCPBars(ui->bat_current_graph->yAxis,ui->bat_current_graph->xAxis);

    //setup solar current graph
    ui->solar_current_graph->yAxis->setRange(0, 1);
    ui->solar_current_graph->xAxis->setRange(0, 100);
    ui->solar_current_graph->yAxis->setVisible(false);

    //setup solar current bar
    solar_current_bar = new QCPBars(ui->solar_current_graph->yAxis,ui->solar_current_graph->xAxis);

    //setup solar current graph 2
    ui->solar_current_graph_2->yAxis->setRange(0, 1);
    ui->solar_current_graph_2->xAxis->setRange(0, 100);
    ui->solar_current_graph_2->yAxis->setVisible(false);

    //setup solar current bar 2
    solar_current_bar_2 = new QCPBars(ui->solar_current_graph_2->yAxis,ui->solar_current_graph_2->xAxis);

    //Setup temperatures graph
    ui->temp_graph->xAxis->setRange(0, 12);
    ui->temp_graph->xAxis->setTickLabels(false);
    ui->temp_graph->xAxis->setSubTicks(false);
    ui->temp_graph->axisRect()->setupFullAxesBox();
    ui->temp_graph->xAxis->setSubTicks(true);
    ui->temp_graph->yAxis->setRange(0, 100);

    //setup temperature bars
    temperature_bars = new QCPBars(ui->temp_graph->xAxis,ui->temp_graph->yAxis);

    //sets temperature bars location
    position = 0.5;
    for(int i = 0;i < x_temperatures.size(); i++){
        x_temperatures[i] = position;
        position += 1;
    }

    //Setup top of bar temperature labels
    position = (0.041666666666666);
    for(int i = 0;i < x_temperatures.size(); i++){
        bar_temperature_label[i] = new QCPItemText(ui->temp_graph);
        bar_temperature_label[i]->setPositionAlignment(Qt::AlignTop|Qt::AlignHCenter);
        bar_temperature_label[i]->setTextAlignment(Qt::AlignHCenter);
        bar_temperature_label[i]->position->setType(QCPItemPosition::ptAxisRectRatio);
        bar_temperature_label[i]->position->setCoords(position, 0.9);
        bar_temperature_label[i]->setText("0");
        position += 2*(0.041666666666666);
    }


    //Setup solar voltages graph
    ui->solar_voltage_graph->xAxis->setRange(0, 5);
    ui->solar_voltage_graph->xAxis->setTickLabels(false);
    ui->solar_voltage_graph->xAxis->setSubTicks(false);
    ui->solar_voltage_graph->axisRect()->setupFullAxesBox();
    ui->solar_voltage_graph->xAxis->setSubTicks(true);
    ui->solar_voltage_graph->yAxis->setRange(0, 100);

    //setup solar voltage bars
    solar_voltage_bars = new QCPBars(ui->solar_voltage_graph->xAxis,ui->solar_voltage_graph->yAxis);

    //sets solar voltage bars location
    position = 0.5;
    for(int i = 0;i < x_SP_voltages.size(); i++){
        x_SP_voltages[i] = position;
        position += 1;
    }

    //Setup top of bar solar panels voltages labels
    position = 0.100000000000;
    for(int i = 0;i < x_SP_voltages.size(); i++){
        bar_solar_voltage_label[i] = new QCPItemText(ui->solar_voltage_graph);
        bar_solar_voltage_label[i]->setPositionAlignment(Qt::AlignTop|Qt::AlignHCenter);
        bar_solar_voltage_label[i]->setTextAlignment(Qt::AlignHCenter);
        bar_solar_voltage_label[i]->position->setType(QCPItemPosition::ptAxisRectRatio);
        bar_solar_voltage_label[i]->position->setCoords(position, 0.9);
        bar_solar_voltage_label[i]->setText("0.000");
        position += 0.200000000000;
    }


    //setup rx led label
    rx_label = new QLabel(this);
    ui->statusBar->addPermanentWidget(rx_label);
    rx_label->setText("Rx:");

    //code for the rx led widget
    rx_led = new LedIndicator();
    ui->statusBar->addPermanentWidget(rx_led);

    //setup tx led label
    tx_label = new QLabel(this);
    ui->statusBar->addPermanentWidget(tx_label);
    tx_label->setText("Tx:");

    //code for the tx led widget
    tx_led = new LedIndicator();
    ui->statusBar->addPermanentWidget(tx_led);

    //code for balancing led
    layout = new QVBoxLayout();

    balancing_led = new LedIndicator();
    regenerating_led = new LedIndicator();
    motorrelay_led = new LedIndicator();
    solarrelay_led = new LedIndicator();
    chargerrelay_led = new LedIndicator();
    layout->setSpacing(20);
    layout->addWidget(balancing_led);
    layout->addWidget(regenerating_led);
    layout->addWidget(motorrelay_led);
    layout->addWidget(solarrelay_led);
    layout->addWidget(chargerrelay_led);



    ui->states_frame->setLayout(layout);

    //setup status bar left status label
    status_label = new QLabel(this);
    ui->statusBar->addPermanentWidget(status_label);
    status_label->setText("Status - Disconnected");

    //setup status bar right com label
    com_label = new QLabel(this);
    ui->statusBar->addPermanentWidget(com_label);
    com_label->setText("Searching...");

    //setup critical parameters boxes
    ui->over_voltage_box->setText("4.300");
    ui->under_voltage_box->setText("2.900");
    ui->fully_charged_voltage_box->setText("4.200");
    ui->fully_discharged_voltage_box->setText("3.000");

    ui->discharge_over_current_box->setText("160.8");
    ui->charge_over_current_box->setText("16.8");
    ui->over_heat_box->setText("80");
    ui->allowed_disbalancing_box->setText("50.0");

    ui->min_cell_voltage_balancing_box->setText("3.700");
    ui->check_balancing_allowed->setChecked(true);

    //read values
    read_values=true;
    read_values_count=0;

    //show images
    show_image();

    counter=0;


}

//Scan the ports for arduino connection or disconnection
void MainWindow::scanports()
{    
    //unexpected disconnection
    if(board->error() || !board->isOpen()){
        //update status bar with disconnected information
        com_label->setText("Searching...");
        status_label->setText("Status - Disconnected");
        //set tx and rx leds off
        rx_led->setState(false);
        tx_led->setState(false);
        resetgui();
        if(!user_disconnect){
            first_connect = true;
        }
    }

    update_board_name();

    //check if ports in the list still valid
    check_port_valid();

    //scan ports and list them
    foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts()){
        //list all ports available
        if(ui->board_list->findText(serialPortInfo.portName()) == -1)
        {
            ui->board_list->addItem(serialPortInfo.portName());
        }

        if(first_connect == true){
            if(serialPortInfo.portName() == ui->board_list->currentText()){
                if(!serialPortInfo.isBusy()){
                    first_connect = false;
                    connect();
                }
            }
        }

    }
}

//Opens board connection
void MainWindow::board_connection()
{
    foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts()){
        if(serialPortInfo.portName() == ui->board_list->currentText()){
            if(!serialPortInfo.isBusy()){
                connect();
            }
        }
    }
}

//Opens board connection
void MainWindow::board_disconnection()
{
    if(board->isOpen()){
        user_disconnect = true;
        board->close();
    }
}

//shows board image
void MainWindow::show_image()
{
    QString directory = QDir::currentPath();
    QPixmap pm(directory + "/teensy32.jpg"); // <- path to image file
    ui->rawdata_box->append(directory);
    ui->label->setPixmap(pm);
    ui->label->setScaledContents(true);
}

void MainWindow::connect()
{
    //make sure every connect function is disconnected
    QObject::disconnect(board, SIGNAL(readyRead()),this,SLOT(readSerial()));

    //make sure arduino is disconnected
    if(board->isOpen()){
        board->close();
    }

    //clean buffers
    receiving = false;
    start_message = false;
    firstBuffer.clear();
    secondBuffer.clear();

    //initiate board connection
    board_is_available = true;
    board_port_name = ui->board_list->currentText();

    //update combo box
    //ui->board_list->addItem("Teensy 3.2");

    //update status bar
    com_label->setText(board_port_name);
    status_label->setText("Status - Connecting...");

    //set port name
    board->setPortName(board_port_name);
    //set serial port bidirectional
    board->open(QSerialPort::ReadWrite);
    //set connection parameters
    board->setBaudRate(QSerialPort::Baud9600);
    board->setDataBits(QSerialPort::Data8);
    board->setFlowControl(QSerialPort::NoFlowControl);
    board->setParity(QSerialPort::NoParity);
    board->setStopBits(QSerialPort::OneStop);

    //Calls readserial() when data is received

    //QObject::connect(board, &QIODevice::readyRead, [=](){readSerial();});
    QObject::connect(board, SIGNAL(readyRead()),this,SLOT(readSerial()));

    //Sets status bar information
    status_label->setText("Status - Connected");
    com_label->setText(board_port_name);
}

//Reads messages from serial port
void MainWindow::readSerial()
{
    QByteArray auxBuffer;
    //reads all data received from board
    auxBuffer = board->readAll();
    //toggle rx led when a byte is received
    rx_led->toggle();
    MainWindow::updategui();

    for(int i = 0;i < auxBuffer.length();i++ ){
        firstBuffer[0] = auxBuffer[i];
        if(firstBuffer[0] == MSG_START_BYTE[0]){
            if(wait_for_data==false){
                secondBuffer.clear();
                firstBuffer.clear();
                wait_for_id=true;
                full_message=false;
            }
        }

        if(firstBuffer[0] == MSG_END_BYTE[0]){
            if(receiving_length_decimal == 0){
                wait_for_data=false;
                full_message=true;
                firstBuffer.clear();
            }
        }

        if(wait_for_id==true && !firstBuffer.isEmpty()){
            if(counter < 2){
               receiving_id.append(firstBuffer);
               counter++;
               firstBuffer.clear();
            }
            else{
                wait_for_id=false;
                wait_for_length=true;
                counter=0;
            }

        }

        if(wait_for_length==true && !firstBuffer.isEmpty()){
            receiving_length.append(firstBuffer);
            receiving_length_decimal = qFromBigEndian<quint8>(receiving_length.data()) + 1;
            firstBuffer.clear();
            wait_for_length=false;
            wait_for_data=true;
            first_byte=true;
        }

        if(wait_for_data==true && !firstBuffer.isEmpty()){
            if(first_byte==true){
                secondBuffer.append(receiving_id);
                secondBuffer.append(receiving_length);
                receiving_length.clear();
                receiving_id.clear();
                first_byte=false;
            }
            secondBuffer.append(firstBuffer);
            firstBuffer.clear();
            if(receiving_length_decimal > 0){
                receiving_length_decimal--;
            }
        }

        /*
        if(firstBuffer[0] == MSG_START_BYTE[0]){
           receiving = true;
           start_message = true;
           secondBuffer.append(firstBuffer);

        }
        else if(firstBuffer[0] == MSG_END_BYTE[0]){
           receiving = false;
        }

        else if(receiving == true && start_message == true){
           secondBuffer.append(firstBuffer);
        }

        if(receiving == false && start_message == true)
        */

        if(full_message==true)
        {
            //Outputs all data received to Debug protocol problems
            ui->rawdata_box->append("Data in : " + (secondBuffer.toHex()).toUpper());

            //store can ID
            QByteArray CAN_ID;
            CAN_ID.append(secondBuffer[0]);
            CAN_ID.append(secondBuffer[1]);

            //qDebug() << CAN_ID;
            //qDebug() << secondBuffer;

            //store Data length
            QByteArray Length;
            Length.append(secondBuffer[2]);
            int data_length = qFromBigEndian<quint8>(Length.data());

            //Store Checksum Byte
            QByteArray Checksum;
            Checksum.append(secondBuffer[data_length+3]);

            //remove checksum
            secondBuffer.remove(data_length+3,1);

            //Compute checksum
            unsigned char Checksum_message_received;
            Checksum_message_received = checksum(secondBuffer);

            if(qFromBigEndian<quint8>(Checksum.data()) == Checksum_message_received){
                //Remove CAN_ID,Length
                secondBuffer.remove(0,3);
                //PARSE DATA
                parse_data(CAN_ID,data_length);
            }
            secondBuffer.clear();
            start_message=false;
        }

        firstBuffer.clear();

    }


}

//Sends messages through serial port
void MainWindow::sendSerial()
{
    //get values from input boxes
    getvaluesdata();

    //checks if board is writable
    if(board->isWritable()){
       //toggle tx led when a message is sent
       tx_led->toggle();
       Sleep(100);
       tx_led->toggle();

       //construct messages

       //message 1 (0x0C)
       QByteArray message_1 = construct_message_1();
       //message 2 (0x0D)
       QByteArray message_2 = construct_message_2();
       //message 3 (0x0E)
       QByteArray message_3 = construct_message_3();

       qDebug() << message_1;
       qDebug() << message_2;
       qDebug() << message_3;

       board->write(message_1);
       board->write(message_2);
       board->write(message_3);

    }else{

       QMessageBox::information(this, "Serial Port Error", "Couldn't write information to the board");
    }
}

//updates gui information
void MainWindow::updategui()
{
    //convert double values to strings
    soc_label_gui = QString::number(soc, 'f', 1);
    battery_voltage_label_gui = QString::number(battery_voltage, 'f', 3);
    bat_current_label_gui = QString::number(y_bat_current[0], 'f', 3);
    solar_current_label_gui = QString::number(y_solar_current[0], 'f', 3);
    pack_temp_label_gui = QString::number(get_highest_temperature(), 'f', 1);

    //update the value displayed on labels
    ui->soc_label->setText("State Of Charge: " + soc_label_gui +" %");
    ui->battery_voltage_label->setText("Battery Voltage: " + battery_voltage_label_gui +" V");
    ui->bat_current_label->setText("Battery Current: " + bat_current_label_gui +" A");
    ui->solar_current_label->setText("Solar Current: " + solar_current_label_gui +" A");
    ui->solar_current_label_2->setText("Solar Current: " + solar_current_label_gui +" A");
    ui->pack_temp_label->setText("Pack Temperature: " + pack_temp_label_gui +" ºC");

    ui->solar_voltage1_label->setText("Panel1 Voltage " + QString::number(y_SP_voltages[0], 'f',3) +" V");
    ui->solar_voltage2_label->setText("Panel2 Voltage " + QString::number(y_SP_voltages[1], 'f',3) +" V");
    ui->solar_voltage3_label->setText("Panel3 Voltage " + QString::number(y_SP_voltages[2], 'f',3) +" V");
    ui->solar_voltage4_label->setText("Panel4 Voltage " + QString::number(y_SP_voltages[3], 'f',3) +" V");
    ui->solar_voltage5_label->setText("Panel5 Voltage " + QString::number(y_SP_voltages[4], 'f',3) +" V");

    //update temperature labels
    updatetemps();

    //Current State Update
    if(charging == true){
        ui->state_label->setText("Current State: Charging");
    }
    if(discharging == true){
        ui->state_label->setText("Current State: Discharging");
    }

    if(balancing == true){
        balancing_led->setState(true);
    }
    else if(balancing == false)
    {
        balancing_led->setState(false);
    }

    if(regenerating == true){
        regenerating_led->setState(true);
    }
    else if(regenerating == false)
    {
        regenerating_led->setState(false);
    }

    if(charger_relay == true){
        chargerrelay_led->setState(true);
        ui->charger_relay_label->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 0, 0);}"));
        ui->charger_relay_label->setText(" (CLOSED) ");
    }
    else if(charger_relay == false)
    {
        chargerrelay_led->setState(false);
        ui->charger_relay_label->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 0, 0);}"));
        ui->charger_relay_label->setText(" (OPEN) ");
    }

    if(solar_relays == true){
        solarrelay_led->setState(true);
        ui->solar_relay_label->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 0, 0);}"));
        ui->solar_relay_label->setText(" (CLOSED) ");
    }
    else if(solar_relays == false)
    {
        solarrelay_led->setState(false);
        ui->solar_relay_label->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 0, 0);}"));
        ui->solar_relay_label->setText(" (OPEN) ");
    }

    if(motor_relay == true){
        motorrelay_led->setState(true);
        ui->motor_relay_label->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 0, 0);}"));
        ui->motor_relay_label->setText(" (CLOSED) ");

    }
    else if(motor_relay == false)
    {
        motorrelay_led->setState(false);
        ui->motor_relay_label->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 0, 0);}"));
        ui->motor_relay_label->setText(" (OPEN) ");

    }

    //Warnings

    //over current
    if(over_current == true){
        ui->current_warning_label->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 0, 0);}"));
        ui->current_warning_label->setText(" Over Current ");
        ui->statusBar->showMessage("Current Warning: Over Current ",10);
    }
    else if(over_current == false)
    {
        ui->current_warning_label->setStyleSheet(QStringLiteral("QLabel{color: rgb(0, 0, 0);}"));
        ui->current_warning_label->setText(" - ");
    }
    //over temperature
    if(over_temperature == true){
        ui->temperature_warning_label->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 0, 0);}"));
        ui->temperature_warning_label->setText(" Over Temperature ");
        //ui->statusBar->showMessage("Temperature Warning: Over Temperature ",10);

    }
    else if(over_temperature == false)
    {
        ui->temperature_warning_label->setStyleSheet(QStringLiteral("QLabel{color: rgb(0, 0, 0);}"));
        ui->temperature_warning_label->setText(" - ");
    }
    //over voltage
    if(over_voltage == true){
        ui->voltage_warning_label->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 0, 0);}"));
        ui->voltage_warning_label->setText(" Over Voltage ");
        //ui->statusBar->showMessage("Voltage Warning: Over Voltage ",10);
    }
    else if(over_voltage == false)
    {
        ui->voltage_warning_label->setStyleSheet(QStringLiteral("QLabel{color: rgb(0, 0, 0);}"));
        ui->voltage_warning_label->setText(" - ");
    }
    //under voltage
    if(under_voltage == true){
        ui->voltage_warning_label->setStyleSheet(QStringLiteral("QLabel{color: rgb(255, 0, 0);}"));
        ui->voltage_warning_label->setText(" Under Voltage ");
        //ui->statusBar->showMessage("Voltage Warning: Under Voltage ",10);
    }
    else if(under_voltage == false)
    {
        ui->voltage_warning_label->setStyleSheet(QStringLiteral("QLabel{color: rgb(0, 0, 0);}"));
        ui->voltage_warning_label->setText(" - ");
    }

    //update graph information
    updategraphs();


}

//plots information in for
void MainWindow::updategraphs()
{
    //update top of bar voltage label value and positon
    double position = 0.041666666666666;
    for(int i = 0;i < x_cells.size(); i++){
        if(y_voltages[i]<2){
            bar_voltage_label[i]->position->setCoords(position, 0.90);
        }else if(y_voltages[i]>4.9){
            bar_voltage_label[i]->position->setCoords(position, 0);
        }else{
            bar_voltage_label[i]->position->setCoords(position, (-0.3329*y_voltages[i]) + 1.5674);
        }
        bar_voltage_label[i]->setText(QString::number(y_voltages[i], 'f', 3));
        position += 2*(0.041666666666666);
    }

    //update top of bar temperature value and position
    position = 0.041666666666666;
    for(int i = 0;i < x_temperatures.size(); i++){
        if(y_temperatures[i]<0){
            bar_temperature_label[i]->position->setCoords(position, 0.90);
        }else if(y_temperatures[i]>92.5){
            bar_temperature_label[i]->position->setCoords(position, 0);
        }else{
            bar_temperature_label[i]->position->setCoords(position, (-0.0097*y_temperatures[i]) + 0.9);
        }
        bar_temperature_label[i]->setText(QString::number(y_temperatures[i], 'f', 0));
        position += 2*(0.041666666666666);
    }

    //update top of bar solar panels voltages value and position
    position = 0.100000000000;
    for(int i = 0;i < x_SP_voltages.size(); i++){
       bar_solar_voltage_label[i]->position->setCoords(position, (-0.0097*y_SP_voltages[i]) + 0.9);
       bar_solar_voltage_label[i]->setText(QString::number(y_SP_voltages[i], 'f', 3));
       position += 0.200000000000;
    }


    //plots voltage graph
    voltage_bars->setData(x_cells,y_voltages);
    voltage_bars->setWidth(0.5);
    voltage_bars->setPen(Qt::NoPen);
    voltage_bars->setBrush(QColor(10, 140, 70, 160));

    //plots balancing bars
    for(int i = 0;i < x_cells.size(); i++){
        if(balancing_cells[i]==true){
           y_voltages_balancing[i] = y_voltages[i];
        }
        else if(balancing_cells[i]==false){
           y_voltages_balancing[i] = 0;
        }
    }

    balancing_bars->setData(x_cells,y_voltages_balancing);
    balancing_bars->setWidth(0.5);
    balancing_bars->setPen(Qt::NoPen);
    balancing_bars->setBrush(QColor(206, 32, 41, 255));

    //plots battery current graph
    bat_current_bar->setData(x_bat_current,y_bat_current);
    bat_current_bar->setWidth(1);
    bat_current_bar->setPen(Qt::NoPen);
    bat_current_bar->setBrush(QColor(10, 140, 70, 160));

    //plots solar current graph
    solar_current_bar->setData(x_solar_current,y_solar_current);
    solar_current_bar->setWidth(1);
    solar_current_bar->setPen(Qt::NoPen);
    solar_current_bar->setBrush(QColor(10, 140, 70, 160));

    //plots solar current graph 2
    solar_current_bar_2->setData(x_solar_current,y_solar_current);
    solar_current_bar_2->setWidth(1);
    solar_current_bar_2->setPen(Qt::NoPen);
    solar_current_bar_2->setBrush(QColor(10, 140, 70, 160));

    //plots temperature graph
    temperature_bars->setData(x_temperatures,y_temperatures);
    temperature_bars->setWidth(0.5);
    temperature_bars->setPen(Qt::NoPen);
    temperature_bars->setBrush(QColor(10, 140, 70, 160));


    //plots solar voltage graph
    solar_voltage_bars->setData(x_SP_voltages,y_SP_voltages);
    solar_voltage_bars->setWidth(0.5);
    solar_voltage_bars->setPen(Qt::NoPen);
    solar_voltage_bars->setBrush(QColor(10, 140, 70, 160));

    //replots all the graphs
    ui->voltage_graph->replot();
    ui->bat_current_graph->replot();
    ui->solar_current_graph->replot();
    ui->solar_current_graph_2->replot();
    ui->temp_graph->replot();
    ui->solar_voltage_graph->replot();
}

//updates gui information
void MainWindow::resetgui()
{
     for(int i = 0; i < y_voltages.size(); i++){
         //y_voltages[i]=0;
     }
     updategraphs();
}

//Update temperature labels
void MainWindow::updatetemps()
{

    ui->cells_internal->setText("Cells Internal Temperature: " + QString::number(y_temperatures[12], 'f',1) + " ºC");
    ui->ambitemp->setText("Ambient Temperature: " + QString::number(ambient_temperature, 'f',1) + " ºC");
    ui->heatsink1->setText("Heat Sink 2 Temperature: " + QString::number(heatsink_temp[0], 'f',1) + " ºC");
    ui->heatsink2->setText("Heat Sink 1 Temperature: " + QString::number(heatsink_temp[1], 'f',1) + " ºC");
    ui->ltctemp->setText("LTC Temperature: " + QString::number(ltc_temp, 'f',1) + " ºC");
}

void MainWindow::parse_data(QByteArray CAN_ID, int data_length){

    if(CAN_ID == STATUS_ID){
        parse_data_status(data_length);
        MainWindow::updategui();
    }
    if(CAN_ID == SOC_V_I_ID){
        parse_data_SOC_voltage_currents(data_length);
        MainWindow::updategui();
    }
    if(CAN_ID == CELLS_0_ID)
    {
        parse_data_cellvoltages0(data_length);
        MainWindow::updategui();
    }
    if(CAN_ID == CELLS_1_ID)
    {
        parse_data_cellvoltages1(data_length);
        MainWindow::updategui();
    }
    if(CAN_ID == CELLS_2_ID)
    {
       parse_data_cellvoltages2(data_length);
       MainWindow::updategui();
    }
    if(CAN_ID == SOLAR_0_ID){
       parse_data_solarpanelvoltages0(data_length);
       MainWindow::updategui();
    }
    if(CAN_ID == SOLAR_1_ID){
       parse_data_solarpanelvoltages1(data_length);
       MainWindow::updategui();
    }
    if(CAN_ID == TEMP_0_ID){
       parse_data_temperatures0(data_length);
       MainWindow::updategui();
    }
    if(CAN_ID == TEMP_1_ID){
       parse_data_temperatures1(data_length);
       MainWindow::updategui();
    }
    if(CAN_ID == TEMP_2_ID){
       parse_data_temperatures2(data_length);
       MainWindow::updategui();
    }

    if(read_values == true){
        if(CAN_ID == VALUES_1_ID){
           parse_data_values1(data_length);
           read_values_count++;
           MainWindow::updategui();
        }
        if(CAN_ID == VALUES_2_ID){
           parse_data_values2(data_length);
           read_values_count++;
           MainWindow::updategui();
        }
        if(CAN_ID == VALUES_3_ID){
           parse_data_values3(data_length);
           read_values_count++;
           MainWindow::updategui();
        }
        if(read_values_count > 2){
            read_values=false;
            read_values_count=0;
            QMessageBox::information(this, "Read Values", "The BMS values were loaded Successfully!");

        }
    }

}

void MainWindow::parse_data_status(int data_length)
{

    for(int i = 0; i < data_length; i++){
        QBitArray data_bits;

        for(int b = 0; b < 8; b++) {
               data_bits.setBit( b, secondBuffer[i] & (1<<(7-b)) );
        }

        //Bit 0
        if(data_bits[7] == true){
            if(i==0){
                charging=true;
            }
            if(i==1){
                //LTC not defined
            }
            if(i==2){
                //LTC not defined
            }
            if(i==3){
                balancing_cells[0]=true;
            }
            if(i==4){
                balancing_cells[8]=true;
            }
            if(i==5){
                over_current=true;
            }


        }
        else if(data_bits[7] == false){
            if(i==0){
                charging=false;
            }
            if(i==1){
                //LTC not defined
            }
            if(i==2){
                //LTC not defined
            }
            if(i==3){
                balancing_cells[0]=false;
            }
            if(i==4){
                balancing_cells[8]=false;
            }
            if(i==5){
                over_current=false;
            }


        }

        //Bit 1
        if(data_bits[6] == true){
            if(i==0){
                discharging=true;
            }
            if(i==1){
                //LTC not defined
            }
            if(i==2){
                //LTC not defined
            }
            if(i==3){
                balancing_cells[1]=true;
            }
            if(i==4){
                balancing_cells[9]=true;
            }
            if(i==5){
                over_temperature=true;
            }
        }
        else if(data_bits[6] == false){
            if(i==0){
                discharging=false;
            }
            if(i==1){
                //LTC not defined
            }
            if(i==2){
                //LTC not defined
            }
            if(i==3){
                balancing_cells[1]=false;
            }
            if(i==4){
                balancing_cells[9]=false;
            }
            if(i==5){
                over_temperature=false;
            }
        }

        //Bit 2
        if(data_bits[5] == true){
            if(i==0){
               regenerating=true;
            }
            if(i==1){
                //LTC not defined
            }
            if(i==2){
                //LTC not defined
            }
            if(i==3){
                balancing_cells[2]=true;
            }
            if(i==4){
                balancing_cells[10]=true;
            }
            if(i==5){
                over_voltage=true;
            }
        }
        else if(data_bits[5] == false){
            if(i==0){
               regenerating=false;
            }
            if(i==1){
                //LTC not defined
            }
            if(i==2){
                //LTC not defined
            }
            if(i==3){
                balancing_cells[2]=false;
            }
            if(i==4){
                balancing_cells[10]=false;
            }
            if(i==5){
                over_voltage=false;
            }
        }

        //Bit 3
        if(data_bits[4] == true){
            if(i==0){
                balancing=true;
            }
            if(i==1){
                //LTC not defined
            }
            if(i==2){
                //LTC not defined
            }
            if(i==3){
                balancing_cells[3]=true;
            }
            if(i==4){
                balancing_cells[11]=true;
            }
            if(i==5){
                under_voltage=true;
            }
        }
        else if(data_bits[4] == false){
            if(i==0){
                balancing=false;
            }
            if(i==1){
                //LTC not defined
            }
            if(i==2){
                //LTC not defined
            }
            if(i==3){
                balancing_cells[3]=false;
            }
            if(i==4){
                balancing_cells[11]=false;
            }
            if(i==5){
                under_voltage=false;
            }
        }

        //Bit 4
        if(data_bits[3] == true){
            if(i==0){
                motor_relay=true;
            }
            if(i==1){
                //LTC not defined
            }
            if(i==2){
                //LTC not defined
            }
            if(i==3){
                balancing_cells[4]=true;
            }
            if(i==4){
                //Balancing Cells 1 not defined
            }
            if(i==5){
                //Warnings not defined
            }
        }
        else if(data_bits[3] == false){
            if(i==0){
                motor_relay=false;
            }
            if(i==1){
                //LTC not defined
            }
            if(i==2){
                //LTC not defined
            }
            if(i==3){
                balancing_cells[4]=false;
            }
            if(i==4){
                //Balancing Cells 1 not defined
            }
            if(i==5){
                //Warnings not defined
            }
        }

        //Bit 5
        if(data_bits[2] == true){
            if(i==0){
                solar_relays=true;
            }
            if(i==1){
                //LTC not defined
            }
            if(i==2){
                //LTC not defined
            }
            if(i==3){
                balancing_cells[5]=true;
            }
            if(i==4){
                //Balancing Cells 1 not defined
            }
            if(i==5){
                //Warnings not defined
            }
        }
        else if(data_bits[2] == false){
            if(i==0){
                solar_relays=false;
            }
            if(i==1){
                //LTC not defined
            }
            if(i==2){
                //LTC not defined
            }
            if(i==3){
                balancing_cells[5]=false;
            }
            if(i==4){
                //Balancing Cells 1 not defined
            }
            if(i==5){
                //Warnings not defined
            }
        }

        //Bit 6
        if(data_bits[1] == true){
            if(i==0){
                charger_relay=true;
            }
            if(i==1){
                //LTC not defined
            }
            if(i==2){
                //LTC not defined
            }
            if(i==3){
                balancing_cells[6]=true;
            }
            if(i==4){
                //Balancing Cells 1 not defined
            }
            if(i==5){
                //Warnings not defined
            }
        }
        else if(data_bits[1] == false){
            if(i==0){
                charger_relay=false;
            }
            if(i==1){
                //LTC not defined
            }
            if(i==2){
                //LTC not defined
            }
            if(i==3){
                balancing_cells[6]=false;
            }
            if(i==4){
                //Balancing Cells 1 not defined
            }
            if(i==5){
                //Warnings not defined
            }
        }

        //Bit 7
        if(data_bits[0] == true){
            if(i==0){
                //Status not defined
            }
            if(i==1){
                //LTC not defined
            }
            if(i==2){
                //LTC not defined
            }
            if(i==3){
                balancing_cells[7]=true;
            }
            if(i==4){
                //Balancing Cells 1 not defined
            }
            if(i==5){
                //Warnings not defined
            }
        }
        else if(data_bits[0] == false){
            if(i==0){
                //Status not defined
            }
            if(i==1){
                //LTC not defined
            }
            if(i==2){
                //LTC not defined
            }
            if(i==3){
                balancing_cells[7]=false;
            }
            if(i==4){
                //Balancing Cells 1 not defined
            }
            if(i==5){
                //Warnings not defined
            }
        }

    }
}

void MainWindow::parse_data_SOC_voltage_currents(int data_length)
{
    int iterator=0;
    for(int i = 0; i < data_length; i+=2){
        QByteArray data_bytes;
        data_bytes.append(secondBuffer[i]);
        data_bytes.append(secondBuffer[i+1]);
        if(iterator == 0){
            double number = qFromBigEndian<quint16>(data_bytes.data());
            number = number/100;
            soc = number;
        }
        if(iterator == 1){
            double number = qFromBigEndian<quint16>(data_bytes.data());
            number = number/10000 * 20;
            battery_voltage = number;
        }
        if(iterator == 2){
            double number = qFromBigEndian<qint16>(data_bytes.data());
            number = number/10;
            y_bat_current[0] = number;
        }
        if(iterator == 3){
            double number = qFromBigEndian<quint16>(data_bytes.data());
            number = number/10;
            y_solar_current[0] = number;
        }
        iterator++;
        data_bytes.clear();
    }

}

void MainWindow::parse_data_cellvoltages0(int data_length)
{
    int iterator=0;
    for(int i = 0; i < data_length; i+=2){
        QByteArray voltage_bytes;
        voltage_bytes.append(secondBuffer[i]);
        voltage_bytes.append(secondBuffer[i+1]);
        double number = qFromBigEndian<quint16>(voltage_bytes.data());
        number = number/10000;
        y_voltages[iterator] = number;
        iterator++;
        voltage_bytes.clear();
    }

}

void MainWindow::parse_data_cellvoltages1(int data_length)
{
    int iterator = 4;
    for(int i = 0; i < data_length; i+=2){
        QByteArray voltage_bytes;
        voltage_bytes.append(secondBuffer[i]);
        voltage_bytes.append(secondBuffer[i+1]);
        double number = qFromBigEndian<quint16>(voltage_bytes.data());
        number = number/10000;
        y_voltages[iterator] = number;
        iterator++;
        voltage_bytes.clear();
    }

}

void MainWindow::parse_data_cellvoltages2(int data_length)
{
    int iterator = 8;
    for(int i = 0; i < data_length; i+=2){
        QByteArray voltage_bytes;
        voltage_bytes.append(secondBuffer[i]);
        voltage_bytes.append(secondBuffer[i+1]);
        double number = qFromBigEndian<quint16>(voltage_bytes.data());
        number = number/10000;
        y_voltages[iterator] = number;
        iterator++;
        voltage_bytes.clear();
    }

}

void MainWindow::parse_data_solarpanelvoltages0(int data_length)
{
    int iterator=0;
    for(int i = 0; i < data_length; i+=2){
        QByteArray SP_voltage_bytes;
        SP_voltage_bytes.append(secondBuffer[i]);
        SP_voltage_bytes.append(secondBuffer[i+1]);
        double number = qFromBigEndian<quint16>(SP_voltage_bytes.data());
        number = number/1000;
        y_SP_voltages[iterator] = number;
        iterator++;
        SP_voltage_bytes.clear();
    }

}

void MainWindow::parse_data_solarpanelvoltages1(int data_length)
{
    int iterator=4;
    for(int i = 0; i < data_length; i+=2){
        QByteArray SP_voltage_bytes;
        SP_voltage_bytes.append(secondBuffer[i]);
        SP_voltage_bytes.append(secondBuffer[i+1]);
        double number = qFromBigEndian<quint16>(SP_voltage_bytes.data());
        number = number/1000;
        y_SP_voltages[iterator] = number;
        iterator++;
        SP_voltage_bytes.clear();
    }

}

void MainWindow::parse_data_temperatures0(int data_length)
{
    int iterator = 0;
    for(int i = 0; i < data_length; i++){
        QByteArray temperature_bytes;
        temperature_bytes.append(secondBuffer[i]);
        double number = qFromBigEndian<quint8>(temperature_bytes.data());
        y_temperatures[iterator] = number;
        iterator++;
        temperature_bytes.clear();
    }

}

void MainWindow::parse_data_temperatures1(int data_length)
{
    int iterator = 8;
    for(int i = 0; i < data_length; i++){
        QByteArray temperature_bytes;
        temperature_bytes.append(secondBuffer[i]);
        double number = qFromBigEndian<quint8>(temperature_bytes.data());
        y_temperatures[iterator] = number;
        iterator++;
        temperature_bytes.clear();
    }
}

void MainWindow::parse_data_temperatures2(int data_length)
{
    int iterator=0;
    for(int i = 0; i < data_length; i++){
        QByteArray temperature_bytes;
        temperature_bytes.append(secondBuffer[i]);
        double number = qFromBigEndian<quint8>(temperature_bytes.data());
        if(iterator == 0){
            ambient_temperature = number;
        }
        if(iterator  == 1){
            heatsink_temp[0]=number;
        }
        if(iterator  == 2){
            heatsink_temp[1]=number;
        }
        if(iterator  == 3){
            ltc_temp=number;
        }
        temperature_bytes.clear();
        iterator++;
    }

}

void MainWindow::parse_data_values1(int data_length)
{
    int iterator=0;
    for(int i = 0; i < data_length; i+=2){
        QByteArray data_bytes;
        data_bytes.append(secondBuffer[i]);
        data_bytes.append(secondBuffer[i+1]);
        if(iterator == 0){
            double number = qFromBigEndian<quint16>(data_bytes.data());
            number = number/10000;
            ui->over_voltage_box->setText(QString::number(number));
        }
        if(iterator == 1){
            double number = qFromBigEndian<quint16>(data_bytes.data());
            number = number/10000;
            ui->under_voltage_box->setText(QString::number(number));
        }
        if(iterator == 2){
            double number = qFromBigEndian<quint16>(data_bytes.data());
            number = number/10000;
            ui->min_cell_voltage_balancing_box->setText(QString::number(number));
        }
        if(iterator == 3){
            double number = qFromBigEndian<quint16>(data_bytes.data());
            ui->allowed_disbalancing_box->setText(QString::number(number));;
        }

        iterator++;
        data_bytes.clear();
    }

}

void MainWindow::parse_data_values2(int data_length)
{
    int iterator=0;
    for(int i = 0; i < data_length; i+=2){
        QByteArray data_bytes;
        data_bytes.append(secondBuffer[i]);
        data_bytes.append(secondBuffer[i+1]);

        if(iterator == 0){
            double number = qFromBigEndian<quint16>(data_bytes.data());
            number = number/10000;
            ui->fully_charged_voltage_box->setText(QString::number(number));
        }
        if(iterator == 1){
            double number = qFromBigEndian<quint16>(data_bytes.data());
            number = number/10000;
            ui->fully_discharged_voltage_box->setText(QString::number(number));
        }

        iterator++;
        data_bytes.clear();
    }
}

void MainWindow::parse_data_values3(int data_length)
{
    int iterator=0;
    for(int i = 0; i < data_length-2; i+=2){
        QByteArray data_bytes;
        data_bytes.append(secondBuffer[i]);
        data_bytes.append(secondBuffer[i+1]);

        if(iterator == 0){
            double number = qFromBigEndian<quint16>(data_bytes.data());
            number = number/10;
            ui->charge_over_current_box->setText(QString::number(number));
        }
        if(iterator == 1){
            double number = qFromBigEndian<quint16>(data_bytes.data());
            number = number/10;
            ui->discharge_over_current_box->setText(QString::number(number));
        }
        iterator++;
        data_bytes.clear();
    }
    QByteArray data_bytes;
    double number;
    data_bytes.append(secondBuffer[data_length-2]);
    number = qFromBigEndian<quint8>(data_bytes.data());
    ui->over_heat_box->setText(QString::number(number));
    data_bytes.clear();

    data_bytes.append(secondBuffer[data_length-1]);
    bool tick = qFromBigEndian<quint8>(data_bytes.data());
    if(tick == true){
        ui->check_balancing_allowed->setChecked(true);
    }
    if(tick == false){
        ui->check_balancing_allowed->setChecked(false);
    }
    data_bytes.clear();
}

void MainWindow::check_port_valid()
{
    bool valid = false;
    for(int i=0; i < ui->board_list->count(); i++)
    {
        valid = false;
        //scan ports
        foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts()){
            //list all ports available
            if(ui->board_list->itemText(i) == serialPortInfo.portName())
            {
                valid=true;
            }

        }
        //remove port from list
        if(valid == false){
            ui->board_list->removeItem(i);
            ui->board_name->setText("");
        }
    }
}

void MainWindow::update_board_name()
{
    //scan ports
    foreach(const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts()){
        //list all ports available
        if(ui->board_list->currentText() == serialPortInfo.portName())
        {
            if(serialPortInfo.hasProductIdentifier() && serialPortInfo.hasVendorIdentifier()){
                if((serialPortInfo.productIdentifier() == 1155) && (serialPortInfo.vendorIdentifier() == 5824)){
                                ui->board_name->setText("Board: Teensy");
                }
                else if((serialPortInfo.productIdentifier() == 67) && (serialPortInfo.vendorIdentifier() == 9025)){
                                ui->board_name->setText("Board: Arduino");
                }
                else{
                   ui->board_name->setText("Board: Undefined");
                }
            }
            else{
                ui->board_name->setText("Board: Undefined");
            }
        }
    }

}

unsigned char MainWindow::checksum(QByteArray message)
{
    unsigned char Checksum_message = '\x00';
    for(int i=0;i<message.length();i++)
    {
        Checksum_message ^= message[i];
    }

    return Checksum_message;

}

void MainWindow::getvaluesdata(){

    //gets data from input boxes
    over_voltage_cutoff = static_cast<int>(round(ui->over_voltage_box->text().toDouble() * 10000));
    under_voltage_cutoff = static_cast<int>(round(ui->under_voltage_box->text().toDouble() * 10000));
    fully_charged_voltage = static_cast<int>(round(ui->fully_charged_voltage_box->text().toDouble() * 10000));
    fully_discharged_voltage = static_cast<int>(round(ui->fully_discharged_voltage_box->text().toDouble() * 10000));
    discharge_overcurrent_cutoff = static_cast<int>(round(ui->discharge_over_current_box->text().toDouble() * 1000));
    charge_overcurrent_cutoff = static_cast<int>(round(ui->charge_over_current_box->text().toDouble() * 1000));
    over_heat_cutoff = static_cast<int>(round(ui->over_heat_box->text().toDouble()));
    allowed_disbalancing = static_cast<int>(round(ui->allowed_disbalancing_box->text().toDouble()));
    min_cell_voltage_balancing = static_cast<int>(round(ui->min_cell_voltage_balancing_box->text().toDouble()));
    balancing_allowed = ui->check_balancing_allowed->checkState();

}

QByteArray MainWindow::construct_message_1(){
    QByteArray message1;
    QByteArray message1_data;

    message1.append(MSG_START_BYTE);

    message1_data.append(VALUES_1_ID);
    message1_data.append(static_cast<char>(8 & 0XFF));

    message1_data.append(static_cast<char>((over_voltage_cutoff >> 8) & 0XFF));
    message1_data.append(static_cast<char>(over_voltage_cutoff & 0XFF));

    message1_data.append(static_cast<char>((under_voltage_cutoff >> 8) & 0XFF));
    message1_data.append(static_cast<char>(under_voltage_cutoff & 0XFF));

    message1_data.append(static_cast<char>((min_cell_voltage_balancing >> 8) & 0XFF));
    message1_data.append(static_cast<char>(min_cell_voltage_balancing & 0XFF));

    message1_data.append(static_cast<char>((allowed_disbalancing >> 8) & 0XFF));
    message1_data.append(static_cast<char>(allowed_disbalancing & 0XFF));

    //Compute checksum
    unsigned char checksum_message_tosend;
    checksum_message_tosend = checksum(message1_data);

    message1.append(message1_data);
    message1.append(static_cast<char>(checksum_message_tosend & 0XFF));
    message1.append(MSG_END_BYTE);

    return message1;

}

QByteArray MainWindow::construct_message_2(){
    QByteArray message2;
    QByteArray message2_data;

    message2.append(MSG_START_BYTE);

    message2_data.append(VALUES_2_ID);
    message2_data.append(static_cast<char>(4 & 0XFF));

    message2_data.append(static_cast<char>((fully_charged_voltage >> 8) & 0XFF));
    message2_data.append(static_cast<char>(fully_charged_voltage & 0XFF));

    message2_data.append(static_cast<char>((fully_discharged_voltage >> 8) & 0XFF));
    message2_data.append(static_cast<char>(fully_discharged_voltage & 0XFF));

    //Compute checksum
    unsigned char checksum_message_tosend;
    checksum_message_tosend = checksum(message2_data);

    message2.append(message2_data);
    message2.append(static_cast<char>(checksum_message_tosend & 0XFF));
    message2.append(MSG_END_BYTE);

    return message2;

}

QByteArray MainWindow::construct_message_3(){
    QByteArray message3;
    QByteArray message3_data;

    message3.append(MSG_START_BYTE);

    message3_data.append(VALUES_3_ID);
    message3_data.append(static_cast<char>(6 & 0XFF));

    message3_data.append(static_cast<char>((discharge_overcurrent_cutoff >> 8) & 0XFF));
    message3_data.append(static_cast<char>(discharge_overcurrent_cutoff & 0XFF));

    message3_data.append(static_cast<char>((charge_overcurrent_cutoff >> 8) & 0XFF));
    message3_data.append(static_cast<char>(charge_overcurrent_cutoff & 0XFF));

    message3_data.append(static_cast<char>(over_heat_cutoff & 0XFF));

    message3_data.append(static_cast<char>(balancing_allowed & 0XFF));

    //Compute checksum
    unsigned char checksum_message_tosend;
    checksum_message_tosend = checksum(message3_data);

    message3.append(message3_data);
    message3.append(static_cast<char>(checksum_message_tosend & 0XFF));
    message3.append(MSG_END_BYTE);
    return message3;
}

void MainWindow::readvalues_enable(){
    read_values = true;
}

double MainWindow::get_highest_temperature(){
    return *std::max_element(y_temperatures.constBegin(), y_temperatures.constEnd());
}
