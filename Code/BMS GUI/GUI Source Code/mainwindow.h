/*
    Copyright (C) 2019  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include <QSerialPort>
#include <QByteArray>
#include <qcustomplot.h>
#include <ledindicator.h>
#include <QtEndian>
#include <Qcolor>

namespace Ui {

class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    //gui functions
    void setupgui();
    void updategui();
    void resetgui();

    //board connection functions
    void scanports();
    void board_connection();
    void connect();
    void board_disconnection();
    void check_port_valid();
    void show_image();
    void update_board_name();


    //plot functions
    void updategraphs();

    //board data functions
    void readSerial();
    void sendSerial();

    //temperature update function
    void updatetemps();

    //parse data function
    void parse_data(QByteArray CAN_ID, int data_length);

    //parse functions
    void parse_data_status(int data_length);
    void parse_data_SOC_voltage_currents(int data_length);
    void parse_data_cellvoltages0(int data_length);
    void parse_data_cellvoltages1(int data_length);
    void parse_data_cellvoltages2(int data_length);
    void parse_data_solarpanelvoltages0(int data_length);
    void parse_data_solarpanelvoltages1(int data_length);
    void parse_data_temperatures0(int data_length);
    void parse_data_temperatures1(int data_length);
    void parse_data_temperatures2(int data_length);
    void parse_data_values1(int data_length);
    void parse_data_values2(int data_length);
    void parse_data_values3(int data_length);

    //construct message functions
    void getvaluesdata();
    QByteArray construct_message_1();
    QByteArray construct_message_2();
    QByteArray construct_message_3();

    void readvalues_enable();

    //checksum funtion
    unsigned char checksum(QByteArray message);

    double get_highest_temperature();

private:

    Ui::MainWindow *ui;
    //CAN ID'S

    QByteArray MSG_START_BYTE;
    QByteArray MSG_END_BYTE;
    QByteArray STATUS_ID;
    QByteArray SOC_V_I_ID;
    QByteArray CELLS_0_ID;
    QByteArray CELLS_1_ID;
    QByteArray CELLS_2_ID;
    QByteArray SOLAR_0_ID;
    QByteArray SOLAR_1_ID;
    QByteArray TEMP_0_ID;
    QByteArray TEMP_1_ID;
    QByteArray TEMP_2_ID;
    QByteArray TEMP_3_ID;

    QByteArray VALUES_1_ID;
    QByteArray VALUES_2_ID;
    QByteArray VALUES_3_ID;

    //board
    QSerialPort *board;
    bool board_is_available;
    QString board_port_name;
    bool first_connect = true;
    bool user_disconnect = false;

    //Read data variables
    QByteArray firstBuffer;
    QByteArray secondBuffer;

    bool receiving=false;
    bool start_message=false;

    static const quint16 arduino_uno_vendor_id = 6790;
    static const quint16 arduino_uno_product_id = 29987;

    //BMS states
    bool charging=false;
    bool discharging=false;
    bool regenerating=false;
    bool balancing=false;
    bool charger_relay=false;
    bool motor_relay=false;
    bool solar_relays=false;


    //BMS Warnings
    bool over_current=false;
    bool over_temperature=false;
    bool over_voltage=false;
    bool under_voltage=false;

    //sets vectors for graph information
    QVector<double> x_cells, y_voltages;
    QVector<double> x_bat_current, y_bat_current;
    QVector<double> x_solar_current, y_solar_current;
    QVector<double> x_temperatures, y_temperatures;
    QVector<double> x_SP_voltages, y_SP_voltages;
    QVector<double> y_voltages_balancing;
    QVector<double> temperatures,heatsink_temp;

    //vector of bolleans for balancing cells
    QVector<bool> balancing_cells;

    //status bar
    QLabel *com_label;
    QLabel *status_label;
    QLabel *rx_label;
    QLabel *tx_label;

    //Voltage graph
    QCPBars *voltage_bars;
    QCPBars *balancing_bars;
    QVector<QCPItemText*> bar_voltage_label;


    //Current graphs
    QCPBars *bat_current_bar;
    QCPBars *solar_current_bar;
    QCPBars *solar_current_bar_2;


    //Temperature graph
    QCPBars *temperature_bars;
    QVector<QCPItemText*> bar_temperature_label;

    //Solar voltage graph
    QCPBars *solar_voltage_bars;
    QVector<QCPItemText*> bar_solar_voltage_label;

    //variables
    QString soc_label_gui;
    QString battery_voltage_label_gui;
    QString pack_temp_label_gui;
    QString bat_current_label_gui;
    QString solar_current_label_gui;

    double battery_voltage;
    double soc;

    //temperature variable
    double ambient_temperature;
    double ltc_temp;

    //leds indicators
    LedIndicator *rx_led;
    LedIndicator *tx_led;
    LedIndicator *balancing_led;
    LedIndicator *regenerating_led;
    LedIndicator *motorrelay_led;
    LedIndicator *solarrelay_led;
    LedIndicator *chargerrelay_led;
    QVBoxLayout *layout;
    
    //cell settings
    int over_voltage_cutoff;
    int under_voltage_cutoff;
    int fully_charged_voltage;
    int fully_discharged_voltage;
    int discharge_overcurrent_cutoff;
    int charge_overcurrent_cutoff;
    int over_heat_cutoff;
    int allowed_disbalancing;
    int min_cell_voltage_balancing;
    bool balancing_allowed;

    //read values from board
    bool read_values;
    int read_values_count;

    int counter;
    bool wait_for_id=false;
    bool wait_for_length=false;
    bool wait_for_data=false;
    bool full_message=false;
    bool first_byte=false;
    QByteArray receiving_length;
    QByteArray receiving_id;
    int receiving_length_decimal = 0;




};

#endif // MAINWINDOW_H
