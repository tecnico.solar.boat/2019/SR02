/*
    Copyright (C) 2019  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/



//Include libraries copied from VESC
#include "VescUart.h"
#include "datatypes.h"
#include "Config.h"
#include "mctrl_torquedo.h"

#define Waiting_time 180000 //time waiting for RPI time message
#define Current_switch_threshold 7
#define MSG_TIME 1
#define VESC_PIN 11
#define MAXIMUM_CURRENT 250.0 
#define MINIMUM_CURRENT 0.0
int time_flag=0;
int reverse_flag=0;

IntervalTimer Motor_timer;
IntervalTimer Blink;

int killswitch_flag = 1;
float motor_current = 0;
CAN_message_t msg;
unsigned long timer;

TSB_CAN tsb_can;
VescUart M1_UART;
VescUart M2_UART;
TSB_TORQUEDO MCTRL_THROTTLE_class;

void send_blink(){
  digitalWrite(13, !digitalRead(13));        
}

void setup() {
    RS485SERIAL.begin(19200); //set rs485 throttle serial
    M1_UART.setSerialPort(&MOTOR1);
    M2_UART.setSerialPort(&MOTOR2);
	MOTOR1.begin(115200);
    MOTOR2.begin(115200);
    pinMode(13, OUTPUT);
    pinMode(VESC_PIN, INPUT);

    //set resolution of analogRead
    analogReadResolution(12);
    analogReadAveraging(16);

    // Throttle stufffffff
    pinMode(6, OUTPUT);
    //Prepare throttle
    MCTRL_THROTTLE_class.PrepareThrottleRequest();

    //CAN begin
    Can0.begin(1000000);
    //Handler for CAN communication
    Can0.attachObj(&tsb_can);
    tsb_can.attachGeneralHandler();
    Motor_timer.begin(send_messages, MSG_TIME*1000000);
    Blink.begin(send_blink, MSG_TIME*500000);
   
    init_messages();
    digitalWrite(13, HIGH);

    // Setup WDT
    noInterrupts();                                         // don't allow interrupts while setting up WDOG
    WDOG_UNLOCK = WDOG_UNLOCK_SEQ1;                         // unlock access to WDOG registers
    WDOG_UNLOCK = WDOG_UNLOCK_SEQ2;
    delayMicroseconds(1);                                   // Need to wait a bit..

    // for this demo, we will use 1 second WDT timeout (e.g. you must reset it in < 1 sec or a boot occurs)
    WDOG_TOVALH = 0x006d;
    WDOG_TOVALL = 0xdd00;

    // This sets prescale clock so that the watchdog timer ticks at 7.2MHz
    WDOG_PRESC  = 0x400;

    // Set options to enable WDT. You must always do this as a SINGLE write to WDOG_CTRLH
    WDOG_STCTRLH |= WDOG_STCTRLH_ALLOWUPDATE |
        WDOG_STCTRLH_WDOGEN | WDOG_STCTRLH_WAITEN |
        WDOG_STCTRLH_STOPEN | WDOG_STCTRLH_CLKSRC;
    interrupts(); 
}
	
void loop() {
    if(!digitalRead(VESC_PIN)){
        killswitch_flag=1;
    }
    motor_current = MCTRL_THROTTLE_class.getDesiredCurrent()*MAXIMUM_CURRENT/1000.0;
    //motor_current = tsb_can.motor.current*MAXIMUM_CURRENT/1000.0;
    if(motor_current < 0) reverse_flag = 1; else reverse_flag = 0;
    switch(killswitch_flag){
        case 0:
            switch(reverse_flag){
                case 0:
                    if(abs(motor_current)>= tsb_can.motor.current_threshold){
                    M1_UART.setCurrent(motor_current/2);
                    M2_UART.setCurrent(motor_current/2);
                    } 
                    else{
                        M1_UART.setCurrent(motor_current);
                        M2_UART.setCurrent(0);
                    }
                    break;
                case 1:
                    M1_UART.setCurrent(motor_current/3);
                    M2_UART.setCurrent(motor_current/3);
                    break;
            } 
            break;           
        case 1:
            if(motor_current==0){
                killswitch_flag = 0;
                M1_UART.setCurrent(0);
                M2_UART.setCurrent(0);
            }
            break;
    }
    M1_UART.getVescValues(0);
    delay(10);
    M2_UART.getVescValues(1);

    noInterrupts();
    WDOG_REFRESH = 0xA602;
    WDOG_REFRESH = 0xB480;
    interrupts();
  }




