# TSB Teensy Motor Code

This code is used to communicate with the VESC 75/30

ATTENTION: Connect only one VESC via Serial and the other with CAN to the first VESC or Teensys will be BURNED during your experiments.

This code need to be modified to work with only one VESC connected bia Serial.

Copyright (C) 2019  Técnico Solar Boat
This repository and its contents  is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat