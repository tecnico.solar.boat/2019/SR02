/*
    Copyright (C) 2019  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "CAN_TSB.h"

int32_t ind = 0;

TSB_CAN::TSB_CAN()
{
  this->bms.cellvoltages = new float[12]();
  this->bms.solar_voltages = new float[5]();
  this->bms.temperatures = new float[17]();
}

void TSB_CAN::send_Message(uint32_t Id, uint8_t len, uint8_t *data)
{
  CAN_message_t msg;
  msg.ext = 0;
  msg.id = Id;
  msg.len = len;
  for(int i=0;i<msg.len;i++){
  msg.buf[i] = data[i];
  }
  Can0.write(msg);
}


void TSB_CAN::receive_Message(CAN_message_t &frame)
{
  switch(frame.id) {
  case 0x01:
    this->motor.current = (int16_t)(frame.buf[1]<<8) | frame.buf[0];
  break;
    case 0x11:
      this->bms.status = frame.buf[0];
      this->bms.ltc_status = (frame.buf[1] << 8) | frame.buf[2];
      this->bms.cells_balanc = (frame.buf[4] << 8) | frame.buf[3];
      this->bms.warnings = frame.buf[5];
    break; 
    case 0x21:
      this->bms.soc = ((frame.buf[0]<<8) | frame.buf[1]) / 100.0;
      this->bms.voltage = (frame.buf[2]<<8) | frame.buf[3];
      this->bms.voltage = this->bms.voltage *20.0/10000.0;
      this->bms.current = (int16_t)((frame.buf[4]<<8) | frame.buf[5]) / 10.0;
      this->bms.solar_current = (int16_t)((frame.buf[6]<<8) | frame.buf[7]) / 10.0;
    break;
    case 0x31:
      for(int i=0; i<4; i++){
        this->bms.cellvoltages[i] = (uint16_t)((frame.buf[i*2]<<8) | frame.buf[i*2+1])/10000.0;
      }
    break;
    case 0x41:
      for(int i=0; i<4; i++){
        this->bms.cellvoltages[i+4] = (uint16_t)((frame.buf[i*2]<<8) | frame.buf[i*2+1])/10000.0;
      }
    break;
    case 0x51:
      for(int i=0; i<4; i++){
        this->bms.cellvoltages[i+8] = (uint16_t)((frame.buf[i*2]<<8) | frame.buf[i*2+1])/10000.0;
      }
    break;
    case 0x61:
      for(int i=0; i<4; i++){
        this->bms.solar_voltages[i] = (uint16_t)((frame.buf[i*2+1]<<8) | frame.buf[i*2])/1000.0;
      }
    break;
    case 0x71:
      this->bms.solar_voltages[4] = (uint16_t)((frame.buf[1]<<8) | frame.buf[0])/1000.0;
    break;
    case 0x81:
      for(int i=0; i<8; i++){
        this->bms.temperatures[i] = frame.buf[i];
      }
    break;
    case 0x91:
      for(int i=0; i<5; i++){
        this->bms.temperatures[i+8] = frame.buf[i];
      }
    break;
    case 0xA1:
      for(int i=0; i<4; i++){
        this->bms.temperatures[i+13] = frame.buf[i];
      }
    break;
    case 0xB1:
      for(int i=0; i<8; i++){
        this->bms.temperatures[i+24] = frame.buf[i];
      }
    break;
    case 0x12:
      ind = 0;
      this->motor.motor1_current = buffer_get_float32(frame.buf,100.0,&ind);
      this->motor.input1_current = buffer_get_float32(frame.buf,100.0,&ind);
    break;
    case 0x22:
      ind = 0;
      this->motor.rpm = buffer_get_int32(frame.buf,&ind);
      this->motor.voltage = buffer_get_float16(frame.buf,10.0,&ind);
      this->motor.temperature1 = buffer_get_float16(frame.buf, 1e1, &ind);
      this->motor.dutycyle = buffer_get_float16(message, 1000.0, &ind);
    break;
    case 0x32:
        ind = 0;
        this->motor.motor2_current = buffer_get_float32(frame.buf,100.0,&ind);
        this->motor.input2_current = buffer_get_float32(frame.buf,100.0,&ind);
    break;
    case 0x42:
        ind = 0;
        this->motor.temperature2 = buffer_get_float16(frame.buf, 1e1, &ind);
        this->motor.mtemperature1 = (int16_t)((frame.buf[3]<<8) | frame.buf[2]);
        this->motor.mtemperature2 = (int16_t)((frame.buf[5]<<8) | frame.buf[4]);
    break;
    case 0x14:
        this->motor.current_threshold = (int16_t)((frame.buf[1]<<8) | frame.buf[0]);
    break;
    case 0x0B:
        this->foils.veloc = (float)((frame.buf[3]<<24) | (frame.buf[2]<<16) | (frame.buf[1]<<8) | frame.buf[0]);
  break;
  }
}


void TSB_CAN::printFrame(CAN_message_t &frame, int mailbox)
{
   Serial.print("ID: ");
   Serial.print(frame.id, HEX);
   Serial.print(" Data: ");
   for (int c = 0; c < frame.len; c++) 
   {
      Serial.print(frame.buf[c], HEX);
      Serial.write(' ');
   }
   Serial.write('\r');
   Serial.write('\n');
}

bool TSB_CAN::frameHandler(CAN_message_t &frame, int mailbox, uint8_t controller)
{
    //printFrame(frame, mailbox);
    receive_Message(frame);
    return true;
}
