/*
    Copyright (C) 2019  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "FlexCAN.h"
#include "buffer.h"

class BMS_Data{

    public:
        uint8_t status;
        uint16_t ltc_status;
        uint16_t cells_balanc;
        uint8_t warnings;

        uint16_t soc;
        float voltage;
        float current;
        float solar_current;

        float *cellvoltages;
        float *solar_voltages;
        float *temperatures;
};

class Foils_Data{

    public:
        float veloc;
        String status;
};

class Motor_Data{

    public:
	float motor1_current;
	float input1_current;
	float motor2_current;
	float input2_current;
	float rpm;
	int voltage;
	int temperature1;
	int temperature2;
	int current;
    int current_threshold;
    int mtemperature1;
    int mtemperature2;
    float dutycycle;
};

class TSB_CAN : public CANListener 
{
    
public:
    int *filters;
    CAN_message_t message;
    BMS_Data bms;
    Foils_Data foils;
    Motor_Data motor;

    TSB_CAN();
    void send_Message(uint32_t Id, uint8_t len, uint8_t *data);
    void receive_Message(CAN_message_t &frame);
    void printFrame(CAN_message_t &frame, int mailbox);
    bool frameHandler(CAN_message_t &frame, int mailbox, uint8_t controller); //overrides the parent version so we can actually do something
};
