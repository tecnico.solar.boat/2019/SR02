# TSB CAN Library Code

This is the code for the CAN cummunication bettween modules inside the boat.

Sory the docs are in Portuguese but Google Translator should be able to help you.


Copyright (C) 2019  Técnico Solar Boat
This repository and its contents  is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat


# Protocolo de Comunicação

> Breve descrição do funcionamento e implementação do código.

[Protocolo antigo](https://gitlab.com/tecnicosb/tsb-es/2018/boat-comms/wikis/TSB-Protocol)

#### Índice

- [Estrutura das mensagens](#Mensagens)
- [Estrutura do ID](#Estrutura do ID)
- [Código](#Código)

<a name="Mensagens"/>

## Estrutura das mensagens
![JC](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/CAN-Bus-frame_in_base_format_without_stuffbits.svg/709px-CAN-Bus-frame_in_base_format_without_stuffbits.svg.png)

No código, só nos temos de preocupar em definir o ID, o DL (tamanho da mensagem) e o DB (conteúdo da mensagem).

<a name="ID"/>

### Estrutura do ID

| Priority bits | Data ID | Source ID |
|:------:|:------:|:------:|
|  2 bits |  6 bits  | 3 bits |


#### Priority bits

- Serve para definir a prioridade das mensagens.
- 00 prioridade máxima
- 11 prioridade mínima

#### Data ID

- Define o tipo de mensagem
- Relativo a cada Node do CAN bus

#### Source ID

- Define qual o Node enviou a mensagem
- Juntamente com o Data ID determina qual a mensagem foi enviada

| Source ID (4 bits) |     Node    |
|:------------------:|:-----------:|
|       0x01          |  Solar BMS  |
|       0x02          |  Foils  |
|       0x03          |  Motor  |

<br/>

##### Heartbeat (Data ID = 0x00)
| Length |  Data  |
|:------:|:-------:|
|    0   | null |

## Solar BMS Messages:
### status (CAN ID: 0x11)
**LEN = 5**

|  Offset |   Type   |        Name       |     Scale     |           Description            |
|:-------:|:--------:|:-----------------:|:-------------:|:--------------------------------:|
|    0    |  uint8_t |       Status      |      -        | Status of the battery            | 
|    1    |  uint8_t |   LTC Status      |      -        | Status of the LTC                |
|    2    |  uint8_t |  Cells Balancing0 |      -        | Which cells are balancing [1:9]  |
|    3    |  uint8_t |  Cells Balancing1 |      -        | Which cells are balancing [10:12]|
|    4    |  uint8_t |     Warnings      |      -        | Warnings                         |

### SOC_Voltage_Currents (CAN ID: 0x21)
**LEN = 8**

|  Offset |   Type   |        Name       |     Scale     |        Description          |
|:-------:|:--------:|:-----------------:|:-------------:|:---------------------------:|
|    0    | uint16_t |  State of Charge  |  SOC * 100    | Battery State of Charge     |
|    2    | uint16_t |      Voltage      |  V * 1000     | Battery pack voltage        |
|    4    | int16_t  |      Current      |  A * 10      | Battery pack current        |
|    6    | uint16_t |  Solar Current    |  A * 1000     | Solar Panels current        |


### cellvoltages0(CAN ID: 0x31)
**LEN = 8**

|  Offset |   Type   |     Name      |     Scale     |        Description        |
|:-------:|:--------:|:-------------:|:-------------:|:-------------------------:|
|    0    | uint16_t |   voltage1    | V * 1000      | cell voltage              |
|    2    | uint16_t |   voltage2    |               | cell voltage              |
|    4    | uint16_t |   voltage3    |               | cell voltage              |
|    6    | uint16_t |   voltage4    |               | cell voltage              |

### cellvoltages1(CAN ID: 0x41)
**LEN = 8**

|  Offset |   Type   |     Name      |     Scale     |        Description        |
|:-------:|:--------:|:-------------:|:-------------:|:-------------------------:|
|    0    | uint16_t |   voltage5    | V * 1000      | cell voltage              |
|    2    | uint16_t |   voltage6    |               | cell voltage              |
|    4    | uint16_t |   voltage7    |               | cell voltage              |
|    6    | uint16_t |   voltage8    |               | cell voltage              |

### cellvoltages2(CAN ID: 0x51)
**LEN = 8**

|  Offset |   Type   |     Name      |     Scale     |        Description        |
|:-------:|:--------:|:-------------:|:-------------:|:-------------------------:|
|    0    | uint16_t |   voltage9    | V * 1000      | cell voltage              |
|    2    | uint16_t |   voltage10   |               | cell voltage              |
|    4    | uint16_t |   voltage11   |               | cell voltage              |
|    6    | uint16_t |   voltage12   |               | cell voltage              |

### SolarPanelVoltages0(CAN ID: 0x61)
**LEN = 8**

|  Offset |   Type   |   Name   |     Scale     |        Description        |
|:-------:|:--------:|:--------:|:-------------:|:-------------------------:|
|    0    | uint16_t |   SP1    | V * 1000      | solar arre 1 voltage      |
|    2    | uint16_t |   SP2    |               | solar arre 2 voltage      |
|    4    | uint16_t |   SP3    |               | solar arre 3 voltage      |
|    6    | uint16_t |   SP4    |               | solar arre 4 voltage      |

### SolarPanelVoltages1(CAN ID: 0x71)
**LEN = 8**

|  Offset |   Type   |   Name   |     Scale     |        Description        |
|:-------:|:--------:|:--------:|:-------------:|:-------------------------:|
|    0    | uint16_t |   SP5    | V * 1000      | solar arre 5 voltage      |

### temperatures0 (CAN ID: 0x81)
**LEN = 8**

|  Offset |   Type   |        Name       |     Scale        |        Description       |
|:-------:|:--------:|:-----------------:|:----------------:|:------------------------:|
|    0    |  uint8_t |   temperature0    |  round to units  | temperature              |
|    1    |  uint8_t |   temperature1    |                  | temperature              |
|    2    |  uint8_t |   temperature2    |                  | temperature              |
|    3    |  uint8_t |   temperature3    |                  | temperature              |
|    4    |  uint8_t |   temperature4    |                  | temperature              |
|    5    |  uint8_t |   temperature5    |                  | temperature              |
|    6    |  uint8_t |   temperature6    |                  | temperature              |
|    7    |  uint8_t |   temperature7    |                  | temperature              |

### temperatures1 (CAN ID: 0x91)
**LEN = 8**

|  Offset |   Type   |        Name       |     Scale        |        Description       |
|:-------:|:--------:|:-----------------:|:----------------:|:------------------------:|
|    0    |  uint8_t |   temperature8    |  round to units  | temperature              |
|    1    |  uint8_t |   temperature9    |                  | temperature              |
|    2    |  uint8_t |   temperature10   |                  | temperature              |
|    3    |  uint8_t |   temperature11   |                  | temperature              |
|    4    |  uint8_t |   temperature12   |                  | temperature              |
|    5    |  uint8_t |   temperature13   |                  | temperature              |
|    6    |  uint8_t |   temperature14   |                  | temperature              |
|    7    |  uint8_t |   temperature15   |                  | temperature              |

### temperatures2 (CAN ID: 0xA1)
**LEN = 8**

|  Offset |   Type   |        Name       |     Scale        |        Description       |
|:-------:|:--------:|:-----------------:|:----------------:|:------------------------:|
|    0    |  uint8_t |   temperature16   |  round to units  | temperature              |
|    1    |  uint8_t |   temperature17   |                  | temperature              |
|    2    |  uint8_t |   temperature18   |                  | temperature              |
|    3    |  uint8_t |   temperature19   |                  | temperature              |
|    4    |  uint8_t |   temperature20   |                  | temperature              |
|    5    |  uint8_t |   temperature21   |                  | temperature              |
|    6    |  uint8_t |   temperature22   |                  | temperature              |
|    7    |  uint8_t |   temperature23   |                  | temperature              |

### temperatures3 (CAN ID: 0xB1)
**LEN = 8**

|  Offset |   Type   |        Name       |     Scale        |        Description       |
|:-------:|:--------:|:-----------------:|:----------------:|:------------------------:|
|    0    |  uint8_t |   temperature24   |  round to units  | temperature              |
|    1    |  uint8_t |   temperature25   |                  | temperature              |
|    2    |  uint8_t |   temperature26   |                  | temperature              |
|    3    |  uint8_t |   temperature27   |                  | temperature              |
|    4    |  uint8_t |   temperature28   |                  | temperature              |
|    5    |  uint8_t |   temperature29   |                  | temperature              |
|    6    |  uint8_t |   temperature30   |                  | temperature              |
|    7    |  uint8_t |   temperature31   |                  | temperature              |


---

<a name="Código"/>

## Código

- **[CAN_TSB](https://gitlab.com/tecnicosb/tsb-es/communications/tree/master/Code/CAN_TSB)**


#### Requisitos

- **[VSCode](https://code.visualstudio.com/)** ou **[Atom](https://atom.io/)**
- **[Platformio](https://platformio.org/)**
- **[Collin80's Flexcan](https://github.com/collin80/FlexCAN_Library)**


### Examplo

```c++
TSB_CAN tsb_can(0,0);

void setup(void)
{
  Can0.attachObj(&exampleClass);
  exampleClass.attachGeneralHandler();
}

void loop(void)
{
  tsb_can.send_Message(0x411,2,data);
  Serial.println(tsb_can.message.buf);
}
```

---

